package com.peep1;


import java.io.IOException;
import java.security.SecureRandom;
import java.util.logging.FileHandler;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication

@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.peep1" })
public class Application extends SpringBootServletInitializer {


	
	public static void main(String[] args) {	

		
        System.out.println("Stared*******************************");
        //this was implemented to send notification whenever application restarts
        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);	
		
	//	EventHolderBean bean = ctx.getBean(EventHolderBean.class);
		
	//	logger.info("Notification Event Processed? - " + bean.getEventFired());
		
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		
		return application.sources(Application.class);
	}

	@Bean
	public SecureRandom secureRandom() {
		return new SecureRandom();
	}

	/*
	 * @Bean public void refresh() { return new Refresh().setCallBack(); }
	 */

	@Bean()
	public ThreadPoolTaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
		taskScheduler.setPoolSize(5);
		return taskScheduler;
	}

	/*
	 * @Bean public static PropertyPlaceholderConfigurer
	 * propertyPlaceholderConfigurer() { PropertyPlaceholderConfigurer ppc = new
	 * PropertyPlaceholderConfigurer(); ppc.setLocations(new Resource[] { new
	 * ClassPathResource("/amazon.properties") }); return ppc; }
	 */

	/*
	 * @Bean public AWSCredentials credential() { return new
	 * BasicAWSCredentials(awsId, awsKey); }
	 * 
	 * @Bean public AmazonS3 s3client() { return new
	 * AmazonS3Client(credential()); }
	 */

	/*
	 * @Bean public JavaMailSender javaMailSender() { JavaMailSenderImpl
	 * mailSender = new JavaMailSenderImpl(); Properties mailProperties = new
	 * Properties(); mailProperties.put("mail.smtp.auth", auth);
	 * mailProperties.put("mail.smtp.starttls.enable", starttls);
	 * mailProperties.put("mail.smtp.starttls.required", true);
	 * mailSender.setJavaMailProperties(mailProperties);
	 * mailSender.setHost(host); mailSender.setPort(port);
	 * mailSender.setProtocol(protocol); mailSender.setUsername(username);
	 * mailSender.setPassword(password); return mailSender; }
	 */
}
