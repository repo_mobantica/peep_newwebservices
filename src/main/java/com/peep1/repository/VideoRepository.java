package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.VideoDetails;

@Transactional
public interface VideoRepository extends MongoRepository<VideoDetails, String> {
	public List<VideoDetails> findByStatus(String status);

	public List<VideoDetails> findByStatusAndActivityId(String string, String activityId);

	
}
