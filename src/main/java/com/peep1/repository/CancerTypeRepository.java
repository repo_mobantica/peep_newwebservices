package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;

@Transactional
public interface CancerTypeRepository extends MongoRepository<CancerType, String> {
	public List<CancerType> findByStatus(String status);

	
}
