package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.QuestionDetails;

@Transactional
public interface QuestionsRepository extends MongoRepository<QuestionDetails, String> {
	public List<QuestionDetails> findByStatus(String status);

	public List<QuestionDetails> findByStatusAndQuestionIdAndActivityId(String string, String questionId,
			String activityId);

	public List<QuestionDetails> findByStatusAndActivityId(String string, String activityId);



	
}
