package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.entity.QuestionDetails;

@Transactional
public interface OptionRepository extends MongoRepository<OptionDetails, String> {
	public List<OptionDetails> findByStatus(String status);

	public List<OptionDetails> findByStatusAndQuestionId(String string, String questionId);

	
}
