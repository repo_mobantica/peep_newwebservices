package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Answers;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.QuestionDetailsUserwise;

@Transactional
public interface QuestionsUserWiseRepository extends MongoRepository<QuestionDetailsUserwise, String> {
	public List<QuestionDetailsUserwise> findByStatus(String status);

	public List<Answers> findByStatusAndActivityIdAndQuestionIdAndUserId(String string, String activityId,
			String questionId, String id);

	
}