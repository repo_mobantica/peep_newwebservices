package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.RedFlagOptionDetails;

@Transactional
public interface RedFlagOptionRepository extends MongoRepository<RedFlagOptionDetails, String> {
	public List<RedFlagOptionDetails> findByStatus(String status);

	public List<RedFlagOptionDetails> findByStatusAndRedFlagQuestionId(String string, String questionId);

	public List<RedFlagOptionDetails> findByStatusAndRedFlagOptionId(String string, String redFlagQuestionId);

	
}
