package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.ActivityDetails;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;

@Transactional
public interface ActivityRepository extends MongoRepository<ActivityDetails, String> {
	public List<ActivityDetails> findByStatus(String status);
	public List<ActivityDetails> findByStatusAndModuleId(String status,String moduleId);
	public List<ActivityDetails> findByStatusAndActivityId(String string, String activityId);
	


	
}