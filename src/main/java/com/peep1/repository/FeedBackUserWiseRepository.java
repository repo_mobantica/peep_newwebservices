package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.FeedBackOption;
import com.peep1.entity.FeedBackUserWise;
import com.peep1.entity.ModuleType;

@Transactional
public interface FeedBackUserWiseRepository extends MongoRepository<FeedBackUserWise, String> {
	public List<FeedBackUserWise> findByStatus(String status);

	
}
