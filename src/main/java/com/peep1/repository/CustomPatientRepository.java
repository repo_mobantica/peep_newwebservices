package com.peep1.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.peep1.entity.PatientInformation;


public interface CustomPatientRepository extends MongoRepository<PatientInformation, String>{
	
	   public PatientInformation findByusernameAndPassword(String username, String password);

}
