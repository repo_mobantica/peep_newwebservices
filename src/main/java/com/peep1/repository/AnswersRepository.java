package com.peep1.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Answers;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.VideoDetailsUserwise;

@Transactional
public interface AnswersRepository extends MongoRepository<Answers, String> {
	public List<Answers> findByStatus(String status);

	

	public List<Answers> findByStatusAndAnswerIdAndQuestionIdAndUserId(String string, String answerId,
			String questionId, String userId);

	public Answers findByQuestionId(String questionId);

	public List<Answers> findByStatusAndUserId(String string, String id);
	public List<Answers> findByStatusAndActivityIdAndQuestionIdAndUserId(String status, String activityId,
			String questionId, String id);



	public List<VideoDetailsUserwise> findByStatusAndActivityIdAndUserId(String status, String activityId, String id);

}
