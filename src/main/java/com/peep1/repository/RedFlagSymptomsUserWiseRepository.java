package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.RedFlagQuestionDetails;
import com.peep1.entity.RedFlagSymptomsUserWise;

@Transactional
public interface RedFlagSymptomsUserWiseRepository extends MongoRepository<RedFlagSymptomsUserWise, String> {
	public List<RedFlagSymptomsUserWise> findByStatus(String status);

	public List<RedFlagSymptomsUserWise> findByStatusAndUserId(String string, String id);

	public List<RedFlagSymptomsUserWise> findByUserId(String id);

	
}
