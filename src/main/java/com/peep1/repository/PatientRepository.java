package com.peep1.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.PatientInformation;

import java.lang.String;
import java.util.List;

@Transactional
public interface PatientRepository extends MongoRepository<PatientInformation, String> {

	//@Query("{ 'username': ?0, 'options.password': ?1}")	
	 PatientInformation findByusernameAndPassword(String username, String password);
	
	List<PatientInformation> findByUsername(String username);

	PatientInformation findByusername(String username);

	PatientInformation findById(String id);
}
