package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.RedFlagQuestionDetails;

@Transactional
public interface RedFlagQuestionsRepository extends MongoRepository<RedFlagQuestionDetails, String> {
	public List<RedFlagQuestionDetails> findByStatus(String status);

	
}
