package com.peep1.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.transaction.annotation.Transactional;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.VideoDetails;
import com.peep1.entity.VideoDetailsUserwise;

@Transactional
public interface VideoUserwiseRepository extends MongoRepository<VideoDetailsUserwise, String> {
	public List<VideoDetailsUserwise> findByStatus(String status);

	public List<VideoDetailsUserwise> findByStatusAndUserId(String string, String id);

	public List<VideoDetailsUserwise> findByStatusAndActivityIdAndUserId(String string, String activityId, String id);

	public List<VideoDetailsUserwise> findByStatusAndActivityIdAndUserIdAndVideoId(String string, String activityId,
			String id, String videoId);

	
}
