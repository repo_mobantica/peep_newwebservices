package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.data.mongodb.core.MongoTemplate;
import com.peep1.entity.CancerType;
import com.peep1.entity.PatientInformation;
import com.peep1.entity.VideoDetails;
import com.peep1.repository.CustomPatientRepository;
import com.peep1.repository.PatientRepository;
import com.peep1.response.LoginDetails;
//import com.peep1.services.EmailService;;

@RestController
@RequestMapping("patient")
public class PatientController {

	@Autowired
	PatientRepository patientRespository;

	@Autowired
	CustomPatientRepository customPatientRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	JavaMailSender emailSender;

	@Autowired
	commonController  common;
	/**
	 * 
	 * 
	 * GET /create  --> Create a new patient information and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
	public Map<String, Object> create(@RequestBody  PatientInformation patientinfo) {

		commonController common;
		String encryptedPassword = null;
		String dcryptedPassword = null;

		int flag=0;

		List<PatientInformation> patientinformationList = patientRespository.findAll();

		try
		{
			for(int i=0;i<patientinformationList.size();i++)
			{

				if(patientinformationList.get(i).getEmailId().equals(patientinfo.getEmailId()))
				{
					flag=1;
					break;
				}
			}
		}
		catch (Exception e) {
			System.out.println("eoorrrr == "+e);
		}

		Map<String, Object> dataMap = new HashMap<String, Object>();

		if(flag==0)
		{


			try {
				common = new commonController();
				encryptedPassword=common.encrypt(patientinfo.getPassword());
				dcryptedPassword=common.decrypt(encryptedPassword);
				// System.out.println(dcryptedPassword);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(" Error"+e);
				e.printStackTrace();
			}		 

			patientinfo.setPassword(encryptedPassword);
			patientinfo.setUsername(patientinfo.getEmailId());		        
			patientinfo.setCreatedDate(new Date());
			patientinfo.setStatus("1");
			patientinfo.setActivation_status("1");
			patientinfo = patientRespository.save(patientinfo);

			patientinfo.setPassword(dcryptedPassword);

			try
			{

				String Msgbody1 = "Hi  "+patientinfo.getFirstName()+"    "+patientinfo.getLastName()+",";
				String Msgbody2 = "\n\n Your account registered successfully.\n You can now login. ";
				String Msgbody3 = "\n You can use further login Credentials : ";
				String Msgbody4 =  "\n\n Username : "+patientinfo.getEmailId()+"\n Password : "+dcryptedPassword;
				String Msgbody5 =  "\n\n Please click on following link to activate your account:";
				//String Msgbody6 =  " \n\n http://peepwebapp.genieiot.in/ActivationMail?emailId="+patientinfo.getEmailId(); // production url
				String Msgbody6 =  " \n\n http://192.168.0.25:8081/PEEP/ActivationMail?emailId="+patientinfo.getEmailId();
				String messege=Msgbody1+Msgbody2+Msgbody3+Msgbody4+Msgbody5+Msgbody6;
				String emilId = patientinfo.getEmailId();
				//EmailService emailservice=new EmailService();
				sendSimpleMessage(patientinfo.getEmailId(),"peepapp18@gmail.com",messege);
				//emailservice.sendLoginCredentialsMail(emilId,"Login Credentials", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+" \n\n\t Please, do not reply to the mail.");
			}
			catch(Exception e)
			{
				System.out.println("Mail Sending Exception = "+e.toString());
			}


			dataMap.put("message", "Patient Information created successfully");
			dataMap.put("status", "1");
			dataMap.put("patientinfo", patientinfo);

		}
		else
		{

			dataMap.put("message", "This email id already present, please login");
			//dataMap.put("status", "1");
			//dataMap.put("patientinfo", patientinfo);

		}



		return dataMap;
	}


	@RequestMapping(method=RequestMethod.POST, value="/login")
	public Map<String, Object> login(@RequestBody  PatientInformation patientinfo) {

		String ecryptedPassword = null;
		PatientInformation p= new PatientInformation();
		try {
			ecryptedPassword=common.encrypt(patientinfo.getPassword());
			// System.out.println("ecryptedPassword"+ecryptedPassword);
			p= patientRespository.findByusernameAndPassword(patientinfo.getUsername(), ecryptedPassword);
			String password=p.getPassword();
			password=common.decrypt(password);
			p.setPassword(password);
			//	System.out.println(p.getUsername()+""+p.getPassword());

			//				 Query query = new Query();
			//				 query.addCriteria(
			//				     new Criteria().andOperator(
			//				         Criteria.where("field1").exists(true),
			//				         Criteria.where("field1").ne(false)
			//				     )
			//				 );
			//				 
			//p= patientRespository.findByUsername(patientinfo.getUsername());


			//				 List<PatientInformation> result = patientRespository.exists(example)(query, PatientInformation.class);
			//				 System.out.println("query - " + query.toString());
			//
			//				 for (Foo foo : result) {
			//				     System.out.println("result - " + foo);
			//				 }
			//				 
			// dcryptedPassword=common.decrypt(encryptedPassword);
			// System.out.println(dcryptedPassword);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
		Map<String, Object> dataMap = new HashMap<String, Object>();

		if(p!=null && p.getStatus().equals("1"))
		{
			//System.out.println(p.getFirstName());
			if(p.getActivation_status().equalsIgnoreCase("1"))
			{

				dataMap.put("message", "Patient login successfully");
				dataMap.put("status", "1");
				dataMap.put("patientinfo", p);
			}
			else
			{
				dataMap.put("message", "Please verify the Email Address then Login");
				dataMap.put("status", "0");
				dataMap.put("patientinfo", p); 
				
			}
		}

		else
		{
			if(p!=null && p.getStatus().equals("0"))
			{
				dataMap.put("message", "Please verify the Email Address then Login");
				dataMap.put("status", "0");
				dataMap.put("patientinfo", p); 
			}else
			{
				dataMap.put("message", "Patient login failed");
				dataMap.put("status", "0");
				dataMap.put("patientinfo", p);  
			}


		}
		return dataMap;
	}



	@RequestMapping(method=RequestMethod.POST, value="/updatePassword")
	public Map<String, Object> updatePassword(@RequestBody  PatientInformation patientinfo) {

		String ecryptedPassword = null;
		PatientInformation p= new PatientInformation();
		try {
			List<PatientInformation> patientInformation = patientRespository.findAll();
			ecryptedPassword=common.encrypt(patientinfo.getPassword());
			// System.out.println("ecryptedPassword   "+ecryptedPassword);
			p= patientRespository.findByusername(patientinfo.getUsername());
			p.setPassword(ecryptedPassword);
			p = patientRespository.save(p);

			String password=p.getPassword();
			password=common.decrypt(password);
			p.setPassword(password);

			//System.out.println(p.getUsername()+"     "+p.getPassword());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
		Map<String, Object> dataMap = new HashMap<String, Object>();

		if(p!=null)
		{
			//System.out.println(p.getFirstName());
			dataMap.put("message", "Password updated successfully");
			dataMap.put("status", "1");
			dataMap.put("patientinfo", p);
		}

		else
		{
			dataMap.put("message", "Fail to update password");
			dataMap.put("status", "0");
			dataMap.put("patientinfo", p);

		}
		return dataMap;
	}


	@RequestMapping(method=RequestMethod.POST, value="/updateProfile")
	public Map<String, Object> updateProfile(@RequestBody  PatientInformation patientinfo) {

		String ecryptedPassword = null;
		PatientInformation p= new PatientInformation();
		try {
			List<PatientInformation> patientInformation = patientRespository.findAll();
			// ecryptedPassword=common.encrypt(patientinfo.getPassword());
			// System.out.println("ecryptedPassword   "+patientinfo.getId());
			p= patientRespository.findById(patientinfo.getId());

			p.setFirstName(patientinfo.getFirstName());
			p.setLastName(patientinfo.getLastName());
			//p.setEmailId(patientinfo.getEmailId());
			//p.setPhoneNUmber(patientinfo.getPhoneNUmber());
			//p.setDateOfBirth(patientinfo.getDateOfBirth());
			p = patientRespository.save(p);

			String password=p.getPassword();
			password=common.decrypt(password);
			p.setPassword(password);
			//	System.out.println(p.getUsername()+"     "+p.getPassword());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
		Map<String, Object> dataMap = new HashMap<String, Object>();

		if(p!=null)
		{
			//System.out.println(p.getFirstName());
			dataMap.put("message", "Profile updated successfully");
			dataMap.put("status", "1");
			dataMap.put("patientinfo", p);
		}

		else
		{
			dataMap.put("message", "Fail to update password");
			dataMap.put("status", "0");
			dataMap.put("patientinfo", p);

		}
		return dataMap;
	}


	public void sendSimpleMessage(String to, String subject, String text) {
		SimpleMailMessage message = new SimpleMailMessage(); 
		message.setTo(to); 
		message.setSubject(subject); 
		message.setText(text);
		emailSender.send(message);
	}



	@RequestMapping(method=RequestMethod.POST, value="/forgotPassword")
	public Map<String, Object> forgotPassword(@RequestBody  PatientInformation patientinfo) {


		commonController common;
		String encryptedPassword = null;
		String dcryptedPassword = null;

		Map<String, Object> dataMap = new HashMap<String, Object>();
		try {
			Query query = new Query();
			List<PatientInformation> PatientDetails = mongoTemplate.find(query.addCriteria(Criteria.where("emailId").is(patientinfo.getEmailId())),PatientInformation.class);

			if(PatientDetails.size()!=0)
			{
				//Send password code


				try
				{
					common = new commonController();
					encryptedPassword=common.encrypt(patientinfo.getPassword());
					dcryptedPassword=common.decrypt(PatientDetails.get(0).getPassword());

					String Msgbody1 = "Hi  "+PatientDetails.get(0).getFirstName()+"    "+PatientDetails.get(0).getLastName()+",";
					String Msgbody2 = "\n\n Your account Username And Password As below ";
					String Msgbody3 = "\n You can use further login Credentials : ";
					String Msgbody4 =  "\n\n Username : "+PatientDetails.get(0).getEmailId()+"\n Password : "+dcryptedPassword;
					//String Msgbody6 =  " \n\n http://peepwebapp.genieiot.in/ActivationMail?emailId="+patientinfo.getEmailId(); // production url
					String messege=Msgbody1+Msgbody2+Msgbody3+Msgbody4;
					String emilId = patientinfo.getEmailId();
					//EmailService emailservice=new EmailService();
					sendSimpleMessage(patientinfo.getEmailId(),"peepapp18@gmail.com",messege);
					//emailservice.sendLoginCredentialsMail(emilId,"Login Credentials", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+" \n\n\t Please, do not reply to the mail.");
				}
				catch(Exception e)
				{
					System.out.println("Mail Sending Exception = "+e.toString());
				}

				dataMap.put("message", "Your Password Has been sent to your mail Id");
				dataMap.put("status", "1");
			}
			else
			{
				dataMap.put("message", "This mail id is not present");
				dataMap.put("status", "0");

			}

		} catch (Exception e) {

			e.printStackTrace();
		}		 

		return dataMap;
	}

}