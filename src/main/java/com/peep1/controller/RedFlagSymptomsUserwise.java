package com.peep1.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.ActivityDetails;
import com.peep1.entity.Answers;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.entity.PatientInformation;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.QuestionDetailsUserwise;
import com.peep1.entity.RedFlagOptionDetails;
import com.peep1.entity.RedFlagQuestionDetails;
import com.peep1.entity.RedFlagSymptomsUserWise;
import com.peep1.entity.VideoDetails;
import com.peep1.entity.VideoDetailsUserwise;
import com.peep1.repository.ActivityRepository;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.OptionRepository;
import com.peep1.repository.QuestionsRepository;
import com.peep1.repository.RedFlagOptionRepository;
import com.peep1.repository.RedFlagQuestionsRepository;
import com.peep1.repository.RedFlagSymptomsUserWiseRepository;
import com.peep1.repository.VideoRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.RedFlagQuestionReport;
import com.peep1.response.RedFlagReport;
import com.peep1.response.ResponceRedFlagSymptomsUserWise;
import com.peep1.response.ResponceWSOption;
import com.peep1.response.ResponceWSQuestions;
import com.peep1.response.ResponceWSVideo;
import com.peep1.response.ResponseActivityData;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseOptionDetails;
import com.peep1.response.ResponseQuestionDetails;
import com.peep1.response.ResponseRedFlagOptionDetails;
import com.peep1.response.ResponseRedFlagQuestinAnswerDetailsUserWise;
import com.peep1.response.ResponseRedFlagQuestionDetails;
import com.peep1.response.ResponseRedFlagSymptomsUserWise;
import com.peep1.response.WsRedFlagQuestionDetailsUserwise;
import com.peep1.response.WsRedFlagSymptomsUserWise;

import ch.qos.logback.core.net.SyslogOutputStream;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */

@RestController
@RequestMapping("/redFlagSymptoms")
public class RedFlagSymptomsUserwise {
	
	@Autowired
	RedFlagSymptomsUserWiseRepository redFlagSymptomsUserWiseRepository;
	
	@Autowired
	ActivityRepository activityRepository;
	@Autowired
	RedFlagQuestionsRepository redFlagQuestionsRepository;
	
	@Autowired
	QuestionsRepository questionsRepository;
	
	@Autowired
	RedFlagOptionRepository redFlagOptionRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/addRedFlagSymptoms")
   	public Map<String, Object> create(@RequestBody  ResponseRedFlagSymptomsUserWise responseRedFlagSymptomsUserWise) {
		
		
		List<WsRedFlagSymptomsUserWise > addRedFlagSymtoms=new ArrayList<>();
		addRedFlagSymtoms=responseRedFlagSymptomsUserWise.getAddRedFlagSymtoms();
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		if(addRedFlagSymtoms.size()!=0)
		{
		
		try
		{
		for (int i = 0; i < addRedFlagSymtoms.size(); i++) {
			try
			{
			RedFlagSymptomsUserWise redFlagSymptomsUserWise=new RedFlagSymptomsUserWise();
			WsRedFlagSymptomsUserWise list = addRedFlagSymtoms.get(i);
			redFlagSymptomsUserWise.setRedFlagQuestionId(list.getRedFlagQuestionId());
			redFlagSymptomsUserWise.setRedFlagAnswerId(list.getRedFlagAnswerId());
			redFlagSymptomsUserWise.setCorrectOptionId(list.getCorrectOptionId());
			redFlagSymptomsUserWise.setStatus("1");
			redFlagSymptomsUserWise.setCreatedDate(new Date());
			redFlagSymptomsUserWise.setUserId(list.getUserId());
			redFlagSymptomsUserWise = redFlagSymptomsUserWiseRepository.save(redFlagSymptomsUserWise);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	
		dataMap.put("symptomsDetails", addRedFlagSymtoms);
		dataMap.put("message", "Symptoms added successfully for red flag");
		dataMap.put("status", "1");
		}
		catch (Exception e) {

			System.out.println("Exception  = = "+e);
		}
		}
		else
		{

			dataMap.put("message", "Error in adding red flag");
			dataMap.put("status", "0");
		}
		
	    return dataMap;
	}

	
	
	@RequestMapping(method=RequestMethod.POST,value="/getAllRedFlagSymptomUserwise")
	public Map<String, Object> getAllRedFlagSymptomUserwise(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			
			if(c.checkLogin(loginDetails))
			{
				PatientInformation p=c.getUserId(loginDetails);
				if(p!=null)
				{
					List<RedFlagSymptomsUserWise> symptomsDetails = redFlagSymptomsUserWiseRepository.findByStatusAndUserId("1",p.getId());
					List<ResponceRedFlagSymptomsUserWise> responseSymptomsDetails=new ArrayList<>();
					for(int i=0;i<symptomsDetails.size();i++)
					{
						ResponceRedFlagSymptomsUserWise objresponse=new ResponceRedFlagSymptomsUserWise();
						objresponse.setRedFlagAnswerId(symptomsDetails.get(i).getRedFlagAnswerId());
						objresponse.setRedFlagQuestionId((symptomsDetails.get(i).getRedFlagQuestionId()));
						objresponse.setCorrectOptionId((symptomsDetails.get(i).getCorrectOptionId()));
						objresponse.setUserId(p.getId());
						responseSymptomsDetails.add(objresponse);
						
					}
					dataMap.put("message", "Symptoms found successfully");
					dataMap.put("status", "1");
					dataMap.put("symptomsDetails", responseSymptomsDetails);
				}
			}
			else
			{
				
				dataMap.put("message", "erro while retriving Symptoms");
				dataMap.put("status", "1");
				dataMap.put("symptomsDetails", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Symptoms");
			dataMap.put("status", "1");
			dataMap.put("symptomsDetails", null);
		}
	    return dataMap;
	}
	
	
	
	
	
	
	
	
	

	@RequestMapping(method=RequestMethod.POST,value="/getRedFlagReport ")
	public Map<String, Object> getRedFlagReport (@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		try
		{
			List<RedFlagQuestionDetails> questionList= redFlagQuestionsRepository.findAll();
			List<RedFlagQuestionDetails> questionList1= redFlagQuestionsRepository.findAll();
			
			List<RedFlagOptionDetails> questionOptionList= redFlagOptionRepository.findAll();

			SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy");
			
			if(c.checkLogin(loginDetails))
			{
				PatientInformation p=c.getUserId(loginDetails);
				if(p!=null)
				{
					//List<RedFlagSymptomsUserWise> symptomsDetails = redFlagSymptomsUserWiseRepository.findByStatusAndUserId("1",p.getId());
					List<RedFlagSymptomsUserWise> symptomsDetails = redFlagSymptomsUserWiseRepository.findByUserId(p.getId());
					
					
					List<RedFlagReport> redflagreportList=new ArrayList<>();
					//List<RedFlagReport> redflagreportList1=new ArrayList<>();
					
					List<RedFlagQuestionReport> redflagquestionreportList = new ArrayList<RedFlagQuestionReport>();
					
					RedFlagQuestionReport redFlagQuestionReport=new RedFlagQuestionReport();
					RedFlagReport redFlagReport=new RedFlagReport();
					for(int i=0;i<questionList.size();i++)
					{
						redflagquestionreportList = new ArrayList<RedFlagQuestionReport>();
						redFlagQuestionReport=new RedFlagQuestionReport();
						redFlagReport=new RedFlagReport();

						redFlagReport.setQuestionId(questionList.get(i).getRedFlagQuestionId());
						redFlagReport.setQuestionName(questionList.get(i).getRedFlagQuestionName());

						for(int j=symptomsDetails.size()-1;j>=0;j--)
						{
							if(questionList.get(i).getRedFlagQuestionId().equalsIgnoreCase(symptomsDetails.get(j).getRedFlagQuestionId()))
							{

								for(int k=0;k<questionOptionList.size();k++)
								{
									if(symptomsDetails.get(j).getRedFlagAnswerId().equalsIgnoreCase(questionOptionList.get(k).getRedFlagOptionId()))
									{
										for(int l=0;l<questionList1.size();l++)
										{
											try {
											if(questionOptionList.get(k).getRedFlagQuestionId().equalsIgnoreCase(questionList1.get(l).getRedFlagQuestionId()))
											{

												redFlagQuestionReport=new RedFlagQuestionReport();
												redFlagQuestionReport.setId(symptomsDetails.get(j).getId());
												redFlagQuestionReport.setOptionName(questionOptionList.get(k).getRedFlagOptionName());
												redFlagQuestionReport.setDate(sdfr.format( symptomsDetails.get(j).getCreatedDate()));
												if(questionList1.get(l).getCorrectOptionId().equalsIgnoreCase(questionOptionList.get(k).getRedFlagOptionName()))
												{
													redFlagQuestionReport.setRedFlagStatus(1);
												}
												else
												{

													redFlagQuestionReport.setRedFlagStatus(0);
												}

												redflagquestionreportList.add(redFlagQuestionReport);
											}
											}catch (Exception e) {
												e.printStackTrace();
												// TODO: handle exception
											}
										}
									}
									
								}
								
							}
						}
						
						if(redflagquestionreportList.size()!=0)
						{
						redFlagReport.setArrayOfReport(redflagquestionreportList);
						redflagreportList.add(redFlagReport);
						}
					}
					
					
					
					
					
					
					
					
				/*	
					
					RedFlagQuestionReport redflagquestionReport=new RedFlagQuestionReport();
					
					RedFlagReport objresponse=new RedFlagReport();
					objresponse.setQuestionId((symptomsDetails.get(0).getRedFlagQuestionId()));
					
					for(int x=0;x<questionList.size();x++)
					{
						if(questionList.get(x).getRedFlagQuestionId().equals(symptomsDetails.get(0).getRedFlagQuestionId()))
						{
							objresponse.setQuestionName(questionList.get(x).getRedFlagQuestionName());
						}
					}
					
					String dateString = null;
					for(int i=0;i<symptomsDetails.size();i++)
					{
						dateString = null;
						redflagquestionReport=new RedFlagQuestionReport();
						if(symptomsDetails.get(0).getRedFlagQuestionId().trim().equals(symptomsDetails.get(i).getRedFlagQuestionId().trim()))
						{
							
							redflagquestionReport.setId(symptomsDetails.get(i).getId());
							
							
							SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy");
							
						     dateString = sdfr.format( symptomsDetails.get(i).getCreatedDate());
							redflagquestionReport.setDate(dateString);
							
							
							for(int l=0;l<questionOptionList.size();l++)
							{
								if(symptomsDetails.get(i).getRedFlagAnswerId().equals(questionOptionList.get(l).getRedFlagOptionId()))
								{
									redflagquestionReport.setOptionName(questionOptionList.get(l).getRedFlagOptionName());
										
								}
								
							}
							
							
							redflagquestionReport.setRedFlagStatus(0);
							
							redflagquestionreportList.add(redflagquestionReport);
							
							redflagquestionReport=new RedFlagQuestionReport();
						}
						
					}

					objresponse.setArrayOfReport(redflagquestionreportList);
					redflagreportList1.add(objresponse);
					
					redflagreportList.addAll(redflagreportList1);
					objresponse=new RedFlagReport();
					int flag=0;
					for(int i=0;i<symptomsDetails.size();i++)
					{
						redflagquestionreportList = new ArrayList<RedFlagQuestionReport>();
						redflagreportList1=new ArrayList<>();
						redflagquestionReport=new RedFlagQuestionReport();
						flag=0;
						for(int j=0;j<redflagreportList.size();j++)
						{
							if(symptomsDetails.get(j).getRedFlagQuestionId().equals(symptomsDetails.get(i).getRedFlagQuestionId()))
							{
								flag=1;
								break;
							}
						}
						if(flag==0)
						{

							objresponse=new RedFlagReport();
							objresponse.setQuestionId((symptomsDetails.get(i).getRedFlagQuestionId()));
							
							
							for(int x=0;x<questionList.size();x++)
							{
								if(questionList.get(x).getRedFlagQuestionId().equals(symptomsDetails.get(i).getRedFlagQuestionId()))
								{
									objresponse.setQuestionName(questionList.get(x).getRedFlagQuestionName());
								}
							}
							
							for(int k=0;k<symptomsDetails.size();k++)
							{
								dateString = null;
								if(symptomsDetails.get(i).getRedFlagQuestionId().trim().equals(symptomsDetails.get(k).getRedFlagQuestionId().trim()))
								{

									redflagquestionReport=new RedFlagQuestionReport();
									
									redflagquestionReport.setId(symptomsDetails.get(k).getId());
									SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy");
									
								     dateString = sdfr.format( symptomsDetails.get(i).getCreatedDate());
									redflagquestionReport.setDate(dateString);
									
									
									for(int l=0;l<questionOptionList.size();l++)
									{
										if(symptomsDetails.get(k).getRedFlagAnswerId().equals(questionOptionList.get(l).getRedFlagOptionId()))
										{
											redflagquestionReport.setOptionName(questionOptionList.get(l).getRedFlagOptionName());
												
										}
										
									}
									
									redflagquestionReport.setRedFlagStatus(1);
									
									redflagquestionreportList.add(redflagquestionReport);
								}
								
							}

							objresponse.setArrayOfReport(redflagquestionreportList);
							redflagreportList1.add(objresponse);
							
							
						}
					
					redflagreportList.addAll(redflagreportList1);
				
					}
					*/
					
					dataMap.put("message", "Symptoms found successfully");
					dataMap.put("status", "1");
					dataMap.put("reportQuestion", redflagreportList);
					
				}
			}
			else
			{
				
				dataMap.put("message", "erro while retriving Symptoms");
				dataMap.put("status", "1");
				dataMap.put("symptomsDetails", null);
			}
		}
		
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Symptoms");
			dataMap.put("status", "1");
			dataMap.put("symptomsDetails", null);
		}
	    return dataMap;
	}
	
	
	
	
	@RequestMapping(method=RequestMethod.POST,value="/getRedFlagPreviousRecords")
	public Map<String, Object> getPreviousRecords(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			
	
			if(c.checkLogin(loginDetails))
			{
				PatientInformation p=c.getUserId(loginDetails);
				if(p!=null)
				{
					List<RedFlagSymptomsUserWise> questionListUserWise = redFlagSymptomsUserWiseRepository.findByStatus("1");
					List<WsRedFlagQuestionDetailsUserwise> questionListUserWise1=new ArrayList<WsRedFlagQuestionDetailsUserwise>();
					ResponseRedFlagQuestinAnswerDetailsUserWise responseQuestionDetails=new ResponseRedFlagQuestinAnswerDetailsUserWise();
					List<RedFlagQuestionDetails> questionList = redFlagQuestionsRepository.findByStatus("1");
					List<RedFlagOptionDetails> optoionList = redFlagOptionRepository.findByStatus("1");
					SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

					for(int i=0;i<questionListUserWise.size();i++)
					{
						
						WsRedFlagQuestionDetailsUserwise questionDetailsUserwise=new WsRedFlagQuestionDetailsUserwise();
						
						for(int j=0;j<questionList.size();j++)
						{
						
						if(questionListUserWise.get(i).getRedFlagQuestionId().equals(questionList.get(j).getRedFlagQuestionId()))
						{
							//System.out.println(questionList.get(i).getRedFlagQuestionId()+" "+questionList.get(i).getRedFlagQuestionName());
							questionDetailsUserwise.setRedFlagQuestionId(questionList.get(j).getRedFlagQuestionId());
							questionDetailsUserwise.setRedFlagQuestionName(questionList.get(j).getRedFlagQuestionName());
							
						}
						if(questionListUserWise.get(i).getRedFlagAnswerId().equals(optoionList.get(j).getRedFlagOptionId()))
						{
							try {
							///System.out.println(questionListUserWise.get(i).getRedFlagAnswerId()+" "+optoionList.get(i).getRedFlagOptionName());
							questionDetailsUserwise.setAnswerId(questionListUserWise.get(i).getRedFlagAnswerId());
							questionDetailsUserwise.setCorrectOptionId(questionListUserWise.get(i).getCorrectOptionId());
							questionDetailsUserwise.setAnswer(optoionList.get(j).getRedFlagOptionName());
							}catch(Exception e)
							{
							}
						}
						
						}
			    	  String result = formater.format(questionListUserWise.get(i).getCreatedDate());
			    	  questionDetailsUserwise.setCreatedDate1(result);
			    	  questionDetailsUserwise.setUserId(questionListUserWise.get(i).getUserId());
			    	  questionDetailsUserwise.setStatus(questionListUserWise.get(i).getStatus());
			    	  questionListUserWise1.add(questionDetailsUserwise);
					}
					dataMap.put("recordList", questionListUserWise1);
					dataMap.put("message", "Red Flag Previous Record Details found successfully");
					dataMap.put("status", "1");
				
				}
			}
			else
			{
				
				dataMap.put("message", "error while retriving Red Flag Previous Record Details");
				dataMap.put("status", "0");
				dataMap.put("recordList", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving  Red Flag Previous Record Details");
			dataMap.put("status", "0");
			dataMap.put("recordList", null);
		}
	    return dataMap;
	}
	

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-module")
	public Map<String, Object> readAll() {
		List<RedFlagSymptomsUserWise> symptomsDetails = redFlagSymptomsUserWiseRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Symptoms Details found successfully");
		dataMap.put("totalSymptoms", symptomsDetails.size());
		dataMap.put("status", "1");
		dataMap.put("symptomsDetails", symptomsDetails);
	    return dataMap;
	}
}
