package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Articles;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.Resource;
import com.peep1.repository.ArticlesRepository;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.ResourceRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseResource;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/resource")
public class ResourceAndArticleController {
	
	@Autowired
	ResourceRepository resourceRepository;
	
	@Autowired
	ArticlesRepository articlesRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  Resource resource) {
		
		resource.setCreatedDate(new Date());
		resource.setStatus("1");
		resource = resourceRepository.save(resource);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Resource created successfully");
		dataMap.put("status", "1");
		dataMap.put("resource", resource);
	    return dataMap;
	}

	
	@RequestMapping(method=RequestMethod.POST,value="/getVideoResources")
	public Map<String, Object> getVideoResources(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			String link="";
			String url;
			if(c.checkLogin(loginDetails))
			{
				List<Resource> resource = resourceRepository.findByStatus("1");
				for(int i=0;i<resource.size();i++)
				{
					link="";
					url=resource.get(i).getVideoUrl();
					String[] parts = url.split("=");
					link="https://img.youtube.com/vi/"+parts[1]+"/1.jpg";
					resource.get(i).setVideoName(link);
				}
				
				
				dataMap.put("message", "Resource found successfully");
				dataMap.put("status", "1");
				dataMap.put("responseResource", resource);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Resource");
				dataMap.put("status", "1");
				dataMap.put("responseResource", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Resource");
			dataMap.put("status", "1");
			dataMap.put("responseResource", null);
		}
	    return dataMap;
	}

	
	@RequestMapping(method=RequestMethod.POST, value="/addArticles")
   	public Map<String, Object> addArticles(@RequestBody  Articles articles) {
		
		articles.setCreatedDate(new Date());
		articles.setStatus("1");
		articles = articlesRepository.save(articles);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Articles added successfully");
		dataMap.put("status", "1");
		dataMap.put("articles", articles);
	    return dataMap;
	}

	
	@RequestMapping(method=RequestMethod.POST,value="/getAllArticleResources")
	public Map<String, Object> getModuleTypes(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			
			if(c.checkLogin(loginDetails))
			{
				List<Articles> articles = articlesRepository.findByStatus("1");
				dataMap.put("message", "Articles found successfully");
				dataMap.put("status", "1");
				dataMap.put("articles", articles);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Articles");
				dataMap.put("status", "1");
				dataMap.put("articles", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Articles");
			dataMap.put("status", "1");
			dataMap.put("articles", null);
		}
	    return dataMap;
	}
	
	
	@RequestMapping("/read-all-module")
	public Map<String, Object> readAll() {
		List<Resource> resource = resourceRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Resource found successfully");
		dataMap.put("totalResource", resource.size());
		dataMap.put("status", "1");
		dataMap.put("resource", resource);
	    return dataMap;
	}
}
