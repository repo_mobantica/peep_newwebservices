package com.peep1.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.entity.PatientInformation;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.QuestionDetailsUserwise;
import com.peep1.entity.RedFlagOptionDetails;
import com.peep1.entity.RedFlagSymptomsUserWise;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.OptionRepository;
import com.peep1.repository.QuestionsRepository;
import com.peep1.repository.QuestionsUserWiseRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponceQuestionDetailsUserwise;
import com.peep1.response.ResponceWSOption;
import com.peep1.response.ResponceWSRedFlagOption;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseQuestinAnswerDetailsUserWise;
import com.peep1.response.ResponseQuestionDetails;
import com.peep1.response.WsQuestionDetailsUserwise;
import com.peep1.response.WsRedFlagSymptomsUserWise;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/questions")
public class QuestionsController {
	
	@Autowired
	QuestionsRepository questionsRepository;
	
	
	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	QuestionsUserWiseRepository questionsUserWiseRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  QuestionDetails questionDetails) {
		
		questionDetails.setCreatedDate(new Date());
		questionDetails.setStatus("1");
		questionDetails = questionsRepository.save(questionDetails);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Questions created successfully");
		dataMap.put("status", "1");
		dataMap.put("questionDetails", questionDetails);
	    return dataMap;
	}

	@RequestMapping(method=RequestMethod.POST, value="/addQuestionAnswer")
   	public Map<String, Object> addQuestionAnswer(@RequestBody  ResponseQuestinAnswerDetailsUserWise responseQuestinAnswerDetailsUserWise) {
		
		List<WsQuestionDetailsUserwise> addQuestionAnswer=new ArrayList<>();
		addQuestionAnswer=responseQuestinAnswerDetailsUserWise.getAddQuestionAnswer();
		
		 for (int i = 0; i < addQuestionAnswer.size(); i++) {
				
			 QuestionDetailsUserwise questionDetailsUserwise=new  QuestionDetailsUserwise();
			 WsQuestionDetailsUserwise list = addQuestionAnswer.get(i);
			 questionDetailsUserwise.setQuestionId(list.getQuestionId());
			 questionDetailsUserwise.setActivityId(list.getActivityId());
			 questionDetailsUserwise.setAnswerId(list.getAnswerId());
			 questionDetailsUserwise.setStatus("1");
			 questionDetailsUserwise.setCreatedDate(new Date());
			 questionDetailsUserwise.setUserId(list.getUserId());
			 questionDetailsUserwise = questionsUserWiseRepository.save(questionDetailsUserwise);
				
			}
	/*	 questionDetailsUserwise.setCreatedDate(new Date());
		questionDetailsUserwise.setStatus("1");
		questionDetailsUserwise = questionsUserWiseRepository.save(questionDetailsUserwise);*/
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Question Answers submitted successfully");
		dataMap.put("status", "1");
		dataMap.put("questionDetails", addQuestionAnswer);
	    return dataMap;
	}

	
	
	@RequestMapping(method=RequestMethod.POST,value="/getQuestionDetailsWithOption")
	public Map<String, Object> getQuestionList(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<QuestionDetails> questionList = questionsRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseQuestionDetails> responseQuestion=new ArrayList<>();
				for(int i=0;i<questionList.size();i++)
				{
					List<ResponceWSOption> wsOptionList = new ArrayList<ResponceWSOption>();
					ResponseQuestionDetails objresponse=new ResponseQuestionDetails();
					objresponse.setQuestionId(questionList.get(i).getQuestionId());
					objresponse.setQuestionName(questionList.get(i).getQuestionName());
					objresponse.setActivityId(questionList.get(i).getActivityId());
					objresponse.setCorrectOptionId(questionList.get(i).getCorrectOptionId());
					List<OptionDetails> optionDetails=optionRepository.findByStatusAndQuestionId("1",questionList.get(i).getQuestionId());
					
					
					for(int y=0;y<optionDetails.size();y++)
					{
						ResponceWSOption responceWSOption=new ResponceWSOption();
						responceWSOption.setOptionId(optionDetails.get(y).getOptionId());
						responceWSOption.setOptionName(optionDetails.get(y).getOptionName());
						
						wsOptionList.add(responceWSOption);
						
					}
					objresponse.setWsOption(wsOptionList);
					responseQuestion.add(objresponse);
					
				}
				dataMap.put("message", "Question Details found successfully");
				dataMap.put("status", "1");
				dataMap.put("questionList", responseQuestion);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Question Details");
				dataMap.put("status", "1");
				dataMap.put("questionList", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Question Details");
			dataMap.put("status", "1");
			dataMap.put("questionList", null);
		}
	    return dataMap;
	}

	
	
	@RequestMapping(method=RequestMethod.POST,value="/getQuestionAnswerUserWise")
	public Map<String, Object> getAllQuestionAnswerlist(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			
	
			if(c.checkLogin(loginDetails))
			{
				PatientInformation p=c.getUserId(loginDetails);
				if(p!=null)
				{
					List<QuestionDetailsUserwise> questionListUserWise = questionsUserWiseRepository.findByStatus("1");
					SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					for(int i=0;i<questionListUserWise.size();i++)
					{
			    	  String result = formater.format(questionListUserWise.get(i).getCreatedDate());
			    	  questionListUserWise.get(i).setCreatedDate1(result);
					}
					dataMap.put("questionAnswerList", questionListUserWise);
					dataMap.put("message", "Question Answer Details found successfully");
					dataMap.put("status", "1");
				
				}
			}
			else
			{
				
				dataMap.put("message", "erro while retriving Question Answer Details");
				dataMap.put("status", "1");
				dataMap.put("questionAnswerList", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Question Answer Details");
			dataMap.put("status", "1");
			dataMap.put("questionAnswerList", null);
		}
	    return dataMap;
	}
	
	

	@RequestMapping(method=RequestMethod.POST,value="/getPreviousRecords")
	public Map<String, Object> getPreviousRecords(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			
			
			if(c.checkLogin(loginDetails))
			{
				PatientInformation p=c.getUserId(loginDetails);
				if(p!=null)
				{
					List<QuestionDetailsUserwise> questionListUserWise = questionsUserWiseRepository.findByStatus("1");
					List<QuestionDetailsUserwise> questionListUserWise1=new ArrayList<QuestionDetailsUserwise>();
					ResponseQuestionDetails responseQuestionDetails=new ResponseQuestionDetails();
					List<QuestionDetails> questionList = questionsRepository.findByStatus("1");
					List<OptionDetails> optoionList = optionRepository.findByStatus("1");
					SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					
					
					for(int i=0;i<questionListUserWise.size();i++)
					{
						QuestionDetailsUserwise questionDetailsUserwise=new QuestionDetailsUserwise();
						
						try
						{
						
						for(int j=0;j<questionList.size();j++)
						{
						
							System.out.println("questionListUserWise.get(i).getQuestionId() = "+questionListUserWise.get(i).getQuestionId()+"questionList.get(j).getQuestionId() "+questionList.get(j).getQuestionId());
							
						if(questionListUserWise.get(i).getQuestionId().equals(questionList.get(j).getQuestionId()))
						{
							questionDetailsUserwise.setQuestionName(questionList.get(j).getQuestionName());
							
						}
						if(questionListUserWise.get(i).getAnswerId().equals(optoionList.get(j).getOptionId()))
						{
							questionDetailsUserwise.setAnswer(optoionList.get(j).getOptionName());
							
						}
						
						}
						
						}
						catch (Exception e) {
						}
			    	  String result = formater.format(questionListUserWise.get(i).getCreatedDate());
			    	  questionDetailsUserwise.setCreatedDate1(result);
			    	  questionListUserWise1.add(questionDetailsUserwise);
					}
					dataMap.put("recordList", questionListUserWise1);
					dataMap.put("message", "Previous Record Details found successfully");
					dataMap.put("status", "1");
				
				}
			}
			else
			{
				
				dataMap.put("message", "error while retriving Previous Record Details");
				dataMap.put("status", "1");
				dataMap.put("recordList", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Previous Record Details");
			dataMap.put("status", "1");
			dataMap.put("recordList", null);
		}
	    return dataMap;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-questions")
	public Map<String, Object> readAll() {
		List<QuestionDetails> questionDetails = questionsRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Question Details found successfully");
		dataMap.put("totalquestions", questionDetails.size());
		dataMap.put("status", "1");
		dataMap.put("questions", questionDetails);
	    return dataMap;
	}
}
