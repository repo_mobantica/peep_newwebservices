package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import com.peep1.entity.ActivityDetails;
import com.peep1.entity.Answers;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.entity.PatientInformation;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.QuestionDetailsUserwise;
import com.peep1.entity.VideoDetails;
import com.peep1.entity.VideoDetailsUserwise;
import com.peep1.repository.ActivityRepository;
import com.peep1.repository.AnswersRepository;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.OptionRepository;
import com.peep1.repository.PatientRepository;
import com.peep1.repository.QuestionsRepository;
import com.peep1.repository.QuestionsUserWiseRepository;
import com.peep1.repository.VideoRepository;
import com.peep1.repository.VideoUserwiseRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ReponseActivityDetails;
//import com.peep1.response.ReponseActivityDetails;
import com.peep1.response.ResponceWSActivity;
import com.peep1.response.ResponceWSOption;
import com.peep1.response.ResponceWSQuestions;
import com.peep1.response.ResponceWSVideo;
import com.peep1.response.ResponseActivityData;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseVideoDetails;


/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@Controller
@RestController
@RequestMapping("/activity")
public class ActivityController {
	
	@Autowired
	ActivityRepository activityRepository;
	
    @Autowired
	PatientRepository patientRespository;
	
	@Autowired
	ModuleTypeRepository moduleTypeRepository;
	
	@Autowired
	VideoRepository videoRepository;
	
	@Autowired
	QuestionsRepository questionRepository;

	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	AnswersRepository answersRepository;
	
	@Autowired
	VideoUserwiseRepository videoUserWiseRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	QuestionsUserWiseRepository questionsUserWiseRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  ActivityDetails activityDetails) {
		
		activityDetails.setStatus("1");
		activityDetails.setCreatedDate(new Date());
		activityDetails = activityRepository.save(activityDetails);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Activity created successfully");
		dataMap.put("status", "1");
		dataMap.put("activityDetails", activityDetails);
	    return dataMap;
	}

	
	
	@RequestMapping(method=RequestMethod.POST,value="/getActivityName")
	public Map<String, Object> getVideoDetails(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<ActivityDetails> activityDetails = activityRepository.findByStatusAndModuleId("1",loginDetails.getModuleId());
			
			if(c.checkLogin(loginDetails))
			{
				List<ReponseActivityDetails> reponseActivityDetails=new ArrayList<>();
				for(int i=0;i<activityDetails.size();i++)
				{
					ReponseActivityDetails objresponse=new ReponseActivityDetails();
					objresponse.setActivityId(activityDetails.get(i).getActivityId());
					objresponse.setActivityName(activityDetails.get(i).getActivityName());
					reponseActivityDetails.add(objresponse);
					
				}
				dataMap.put("message", "Activity Details found successfully");
				dataMap.put("status", "1");
				dataMap.put("activityDetails", reponseActivityDetails);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Video Details");
				dataMap.put("status", "1");
				dataMap.put("activityDetails", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Video Details");
			dataMap.put("status", "1");
			dataMap.put("activityDetails", null);
		}
	    return dataMap;
	}
	

	@RequestMapping(method=RequestMethod.POST,value="/getActivityDetails")
	public Map<String, Object> getActivityDetails(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String ecryptedPassword = null;
		try
		{
			
			 ecryptedPassword=c.encrypt(loginDetails.getPassword());
			 
			List<ActivityDetails> activityDetails = activityRepository.findByStatusAndActivityId("1",loginDetails.getActivityId());
			List<VideoDetails> videoDetails=videoRepository.findByStatusAndActivityId("1",loginDetails.getActivityId());
			List<QuestionDetails> questionsDetails=questionRepository.findByStatusAndActivityId("1",loginDetails.getActivityId());
			 PatientInformation p= patientRespository.findByusernameAndPassword(loginDetails.getUsername(), ecryptedPassword);
			
			 for(int i=0;i<videoDetails.size();i++)
				{
					String link="";
					String url=videoDetails.get(i).getVideoLink();
					String[] parts = url.split("=");
					link="https://img.youtube.com/vi/"+parts[1]+"/1.jpg";
					videoDetails.get(i).setVideoImage(link);;
				}
				
				
			ResponseActivityData responceWSActivity=new ResponseActivityData();
			if(c.checkLogin(loginDetails))
			{
				List<ResponseActivityData> responseActivityData=new ArrayList<>();
				List<ResponceWSVideo> wsVideoList = new ArrayList<ResponceWSVideo>();
				List<ResponceWSQuestions> wsQuestionList = new ArrayList<ResponceWSQuestions>();
				List<ResponceWSOption> wsOptionList = new ArrayList<ResponceWSOption>();
				List<ResponseActivityData> wsActivityList=new ArrayList<ResponseActivityData>();
				 List<VideoDetailsUserwise> videoDetailsUserwise=new ArrayList<VideoDetailsUserwise>();
				 List<Answers> answerList=new  ArrayList<Answers>();
				//System.out.println(videoDetails.size()+ " "+questionsDetails.size()+ " "+ activityDetails.size());
			
				int watchQuestionCount=0;
				int watchVideoCount=0;
				for(int i=0;i<activityDetails.size();i++)
				{
					
					for(int k=0;k<videoDetails.size();k++)
					{
						ResponceWSVideo responceWSVideo=new ResponceWSVideo();
						responceWSVideo.setVideoId(videoDetails.get(k).getVideoId());
						responceWSVideo.setVideoName(videoDetails.get(k).getVideoName());
						responceWSVideo.setStatus(videoDetails.get(k).getStatus());
						responceWSVideo.setVideoLink(videoDetails.get(k).getVideoLink());
						responceWSVideo.setVideoImage(videoDetails.get(k).getVideoImage());
						
						videoDetailsUserwise = videoUserWiseRepository.findByStatusAndActivityIdAndUserIdAndVideoId("1",activityDetails.get(i).getActivityId(),p.getId(),videoDetails.get(k).getVideoId());
						//videoDetailsUserwise = videoUserWiseRepository.findByStatusAndActivityIdAndUserIdAndVideoId("1",activityDetails.get(i).getActivityId(),p.getId());
						 if(videoDetailsUserwise.size()>0)
						 {
							 watchVideoCount=watchVideoCount+1;
							 responceWSVideo.setVideoFlag("1");
						 }else
						 {
							 responceWSVideo.setVideoFlag("0");
						 }
						wsVideoList.add(responceWSVideo);
						
					}
					for(int k=0;k<questionsDetails.size();k++)
					{
						ResponceWSQuestions responceWSQuestions=new ResponceWSQuestions();
						responceWSQuestions.setQuestionId(questionsDetails.get(k).getQuestionId());
						responceWSQuestions.setQuestionName(questionsDetails.get(k).getQuestionName());
						responceWSQuestions.setCorrectOptionId(questionsDetails.get(k).getCorrectOptionId());
						List<OptionDetails> optionDetails=optionRepository.findByStatusAndQuestionId("1",questionsDetails.get(k).getQuestionId());
						
						
						answerList=new  ArrayList<Answers>();
						answerList = questionsUserWiseRepository.findByStatusAndActivityIdAndQuestionIdAndUserId("1",activityDetails.get(i).getActivityId(),questionsDetails.get(k).getQuestionId(),p.getId());
						
						
						
						
						for(int j=0;j<answerList.size();j++)
						{
							responceWSQuestions.setAnswerId(answerList.get(j).getAnswerId());
						}
						
							 if(answerList.size()>0)
							 {
								 watchQuestionCount=watchQuestionCount+1;
								 
							 }
							 wsOptionList = new ArrayList<ResponceWSOption>();
							
						//System.out.println(optionDetails.size());
						for(int y=0;y<optionDetails.size();y++)
						{
							
							ResponceWSOption responceWSOption=new ResponceWSOption();
							responceWSOption.setOptionId(optionDetails.get(y).getOptionId());
							responceWSOption.setOptionName(optionDetails.get(y).getOptionName());
							
							wsOptionList.add(responceWSOption);
						}
						responceWSQuestions.setWsOption(wsOptionList);
						wsQuestionList.add(responceWSQuestions);
						
					}
					responceWSActivity.setActivityId(activityDetails.get(i).getActivityId());
					responceWSActivity.setActivityName(activityDetails.get(i).getActivityName());
					responceWSActivity.setModuleId(activityDetails.get(i).getModuleId());
					responceWSActivity.setVideoCount(videoDetails.size());
					responceWSActivity.setQuestionCount(questionsDetails.size());
					responceWSActivity.setWatchVideoCount(watchVideoCount);
					responceWSActivity.setWatchQuestionCount(watchQuestionCount);
					responceWSActivity.setWsVideo(wsVideoList);
					responceWSActivity.setWsQuestions(wsQuestionList);
					wsActivityList.add(responceWSActivity);
					
					
				}
			/*	objresponse.setPageNo(loginDetails.getPageNo());
				objresponse.setWsActivity(wsActivityList);*/
			
				
				
				dataMap.put("message", "Activity found successfully");
				dataMap.put("status", "1");
				dataMap.put("pageNo", "1");
				dataMap.put("activityList",responceWSActivity);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Activities");
				dataMap.put("status", "1");
				dataMap.put("activityList", responceWSActivity);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Activities");
			dataMap.put("status", "1");
			dataMap.put("cancerType", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-module")
	public Map<String, Object> readAll() {
		List<ActivityDetails> activities = activityRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Activities found successfully");
		dataMap.put("totalactivites", activities.size());
		dataMap.put("status", "1");
		dataMap.put("activities", activities);
	    return dataMap;
	}
	
	@RequestMapping("/getModuleWiseActivity")
	public Map<String, Object> getModuleWiseActivity(@RequestBody LoginDetails loginDetails) {
		List<ActivityDetails> activities = activityRepository.findByStatusAndModuleId("1", loginDetails.getModuleId());
		String ecryptedPassword = null;
	
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try {
			commonController common = new commonController();
			 ecryptedPassword=common.encrypt(loginDetails.getPassword());
		//	 System.out.println(loginDetails.getUsername());
			 PatientInformation p= patientRespository.findByusernameAndPassword(loginDetails.getUsername(), ecryptedPassword);

				int videoCount=0;
				int questionCount=0;
				int watchQuestionCount=0;
				int watchVideoCount=0;
				int totalquestionCount=0;
				
				int totalPer=0;
				int finalTotalPer=0;
				int moduleWisePer=0;
				
			 List<VideoDetails> videoDetails=new ArrayList<VideoDetails>();
			 List<QuestionDetails>questionDetails=new ArrayList<QuestionDetails>();
			 List<Answers> answerList=new  ArrayList<Answers>();
			 
			 List<VideoDetailsUserwise> videoDetailsUserwise=new ArrayList<VideoDetailsUserwise>();
				for(int k=0;k<activities.size();k++)
				{
					totalPer=0;
					 videoCount=0;
					 questionCount=0;
					 watchQuestionCount=0;
					 watchVideoCount=0;
					 
					videoDetails = videoRepository.findByStatusAndActivityId("1", activities.get(k).getActivityId());
					 questionDetails = questionRepository.findByStatusAndActivityId("1", activities.get(k).getActivityId());
					 
					 if(videoDetails.size()>0)
					 {
						 videoCount=videoDetails.size();
					 }
					activities.get(k).setVideoCount(videoCount);
					 if(questionDetails.size()>0)
					 {
						 questionCount=questionDetails.size();
					 }
					activities.get(k).setQuestionCount(questionCount);
					totalquestionCount=totalquestionCount+questionCount;
		
			
			for(int y=0;y<questionDetails.size();y++)
			{
				 answerList = questionsUserWiseRepository.findByStatusAndActivityIdAndQuestionIdAndUserId("1",activities.get(k).getActivityId(),
						questionDetails.get(y).getQuestionId(),p.getId());
				 if(answerList.size()>0)
				 {
					 watchQuestionCount=watchQuestionCount+1;
				 }
				 
				 //activities.get(k).setWatchQuestionCount(watchQuestionCount);
				 
			} 
				videoDetailsUserwise = videoUserWiseRepository.findByStatusAndActivityIdAndUserId("1",activities.get(k).getActivityId(),p.getId());
				 if(videoDetailsUserwise.size()>0)
				 {
					 watchVideoCount=videoDetailsUserwise.size();
				 }
				 
						//activities.get(k).setWatchVideoCount(watchVideoCount);
			
			 activities.get(k).setWatchQuestionCount(watchQuestionCount);
			 activities.get(k).setWatchVideoCount(watchVideoCount);
			try {
			if((videoCount+questionCount)!=0)
			{
				totalPer=(100*(watchQuestionCount+watchVideoCount)/(videoCount+questionCount)) ;
				finalTotalPer=finalTotalPer+totalPer;
				
			}
			}catch (Exception e) {
				System.out.println(" error  = "+e);
			}
			activities.get(k).setTotalPer(totalPer);
		}
				if(activities.size()!=0)
				{
				moduleWisePer=finalTotalPer/activities.size();
				}
		
				
		dataMap.put("totalquestionCount", totalquestionCount);
		dataMap.put("moduleWisePer", moduleWisePer);		
		dataMap.put("message", "Activities found successfully");
		dataMap.put("totalactivites", activities.size());
		dataMap.put("status", "1");
		dataMap.put("activities", activities);
	    
		
		}catch(Exception e)
		{
			
		}
		return dataMap;
	}
}
