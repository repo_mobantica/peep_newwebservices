package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Answers;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.repository.AnswersRepository;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/answers")
public class AnswerController {
	
	@Autowired
	AnswersRepository answersRepository;
	
	@Autowired
	commonController c;
	@Autowired
	MongoTemplate mongoTemplate;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/submitAnswer")
   	public Map<String, Object> create(@RequestBody  Answers answers) 
	{
		try
		{
			//	System.out.println(answers.getAnswerId()+ " "+answers.getQuestionId()+ " "+answers.getUserId());
			
			
			List<Answers> i = answersRepository.findByStatusAndAnswerIdAndQuestionIdAndUserId("1",answers.getAnswerId()
					,answers.getQuestionId(),answers.getUserId());
			
		//	System.out.println("size"+i.size());
			
			if(i.size()>0)
			{
			//	System.out.println(answers.getQuestionId());
				
				
				Answers answers1 = answersRepository.findByQuestionId(answers.getQuestionId());
				
			//	System.out.println("status"+answers1.getId());
				//answers1.setStatus("0");
				answers1 = answersRepository.save(answers1);
				
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("message", "Answer updated successfully ");
				dataMap.put("status", "1");
				dataMap.put("answer", answers1);
			    return dataMap;
			}else
			{
				answers.setCreatedDate(new Date());
				answers.setStatus("1");
				answers = answersRepository.save(answers);
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("message", "Answer Submitted successfully ");
				dataMap.put("status", "1");
				dataMap.put("answer", answers);
			    return dataMap;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

//read active cancer types
	@RequestMapping(method=RequestMethod.POST,value="/getAnswerListByUser")
	public Map<String, Object> getModuleTypes(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<Answers> moduleType = answersRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseModuleType> responseModuleType=new ArrayList<>();
				for(int i=0;i<moduleType.size();i++)
				{
					ResponseModuleType objresponse=new ResponseModuleType();
					//objresponse.setModuleType(moduleType.get(i).getModuleType());
					objresponse.setId(moduleType.get(i).getId());
					
					responseModuleType.add(objresponse);
					
				}
				dataMap.put("message", "Module Type found successfully");
				dataMap.put("status", "1");
				dataMap.put("moduleType", responseModuleType);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Module Type");
				dataMap.put("status", "1");
				dataMap.put("moduleType", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Module Type");
			dataMap.put("status", "1");
			dataMap.put("moduleType", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-module")
	public Map<String, Object> readAll() {
		List<Answers> moduleType = answersRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Module Type found successfully");
		dataMap.put("totalmoduleType", moduleType.size());
		dataMap.put("status", "1");
		dataMap.put("moduleType", moduleType);
	    return dataMap;
	}
}
