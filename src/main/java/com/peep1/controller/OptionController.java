package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.OptionRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseOptionDetails;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */

@RestController
@RequestMapping("/option")
public class OptionController {
	
	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  OptionDetails optionDetails) {
		
		optionDetails.setCreatedDate(new Date());
		optionDetails.setStatus("1");
		optionDetails = optionRepository.save(optionDetails);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Option Details created successfully");
		dataMap.put("status", "1");
		dataMap.put("optionDetails", optionDetails);
	    return dataMap;
	}

//read active cancer types
	@RequestMapping(method=RequestMethod.POST,value="/getOptionDetails")
	public Map<String, Object> getOptionDetails(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<OptionDetails> optionDetails = optionRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseOptionDetails> responseOptionDetails=new ArrayList<>();
				for(int i=0;i<optionDetails.size();i++)
				{
					ResponseOptionDetails objresponse=new ResponseOptionDetails();
					objresponse.setOptionId(optionDetails.get(i).getOptionId());
					objresponse.setOptionName((optionDetails.get(i).getOptionName()));
					objresponse.setQuestionId((optionDetails.get(i).getQuestionId()));
					responseOptionDetails.add(objresponse);
					
				}
				dataMap.put("message", "Option Details found successfully");
				dataMap.put("status", "1");
				dataMap.put("optionDetails", responseOptionDetails);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Option Details");
				dataMap.put("status", "1");
				dataMap.put("optionDetails", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Option Details");
			dataMap.put("status", "1");
			dataMap.put("optionDetails", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-module")
	public Map<String, Object> readAll() {
		List<OptionDetails> optionDetails = optionRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Option Details found successfully");
		dataMap.put("totalOptions", optionDetails.size());
		dataMap.put("status", "1");
		dataMap.put("optionDetails", optionDetails);
	    return dataMap;
	}
}
