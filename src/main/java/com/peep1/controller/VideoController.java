package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.PatientInformation;
import com.peep1.entity.VideoDetails;
import com.peep1.entity.VideoDetailsUserwise;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.VideoRepository;
import com.peep1.repository.VideoUserwiseRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponceVideoDetailsUserwise;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseVideoDetails;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;


/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/video")
public class VideoController {
	
	@Autowired
	VideoRepository videoRepository;
	
	@Autowired
	VideoUserwiseRepository videoUserWiseRepository;
	
	@Autowired
	commonController c;

	@Autowired
	MongoTemplate mongoTemplate;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  VideoDetails videoDetails) {
		
		videoDetails.setCreatedDate(new Date());
		videoDetails.setStatus("1");
		//videoDetails.setVideoName(videoName);
		videoDetails = videoRepository.save(videoDetails);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Video created successfully");
		dataMap.put("status", "1");
		dataMap.put("videoDetails", videoDetails);
	    return dataMap;
	}

	@RequestMapping(method=RequestMethod.POST, value="/addWatchVideo")
   	public Map<String, Object> addWatchVideo(@RequestBody  VideoDetailsUserwise videoDetailsUserwise) {
		videoDetailsUserwise.setCreatedDate(new Date());
		videoDetailsUserwise.setStatus("1");
		videoDetailsUserwise = videoUserWiseRepository.save(videoDetailsUserwise);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Video Marked As Read");
		dataMap.put("status", "1");
		dataMap.put("videoDetails", videoDetailsUserwise);
	    return dataMap;
	}

	
	

	@RequestMapping(method=RequestMethod.POST,value="/getUserWatchVideoDetails")
	public Map<String, Object> getUserWatchVideoDetails(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			Query query=new Query();
			if(c.checkLogin(loginDetails))
			{
				PatientInformation p=c.getUserId(loginDetails);
				if(p!=null)
				{
					//System.out.println("hiiii"+p.getId());
					List<VideoDetailsUserwise> videoDetails = videoUserWiseRepository.findByStatusAndUserId("1",p.getId());
					List<ResponceVideoDetailsUserwise> responseVideoDetails=new ArrayList<>();
					for(int i=0;i<videoDetails.size();i++)
					{
						ResponceVideoDetailsUserwise objresponse=new ResponceVideoDetailsUserwise();
						objresponse.setVideoId(videoDetails.get(i).getVideoId());
						
						query=new Query();
				 	    List<VideoDetails> videoDetails1 = mongoTemplate.find(query.addCriteria(Criteria.where("videoId").is(videoDetails.get(i).getVideoId())), VideoDetails.class);
						
				 	    objresponse.setVideoLink(videoDetails1.get(0).getVideoLink());
						objresponse.setUserId(p.getId());
						objresponse.setActivityId(videoDetails.get(i).getActivityId());
						//objresponse.setStatus("1");
						responseVideoDetails.add(objresponse);
						
					}
					dataMap.put("message", "Video Details found successfully");
					dataMap.put("status", "1");
					dataMap.put("videoDetails", responseVideoDetails);
					}
			}
			else
			{
				
				dataMap.put("message", "erro while retriving Video Details");
				dataMap.put("status", "1");
				dataMap.put("videoDetails", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Video Details");
			dataMap.put("status", "1");
			dataMap.put("videoDetails", null);
		}
	    return dataMap;
	}
	
	
	

	@RequestMapping(method=RequestMethod.POST,value="/getVideoDetails")
	public Map<String, Object> getVideoDetails(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<VideoDetails> videoDetails = videoRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseVideoDetails> responseVideoDetails=new ArrayList<>();
				for(int i=0;i<videoDetails.size();i++)
				{
					ResponseVideoDetails objresponse=new ResponseVideoDetails();
					objresponse.setVideoId(videoDetails.get(i).getVideoId());
					objresponse.setVideoLink(videoDetails.get(i).getVideoLink());
					objresponse.setActivityId(videoDetails.get(i).getActivityId());
					//objresponse.setStatus("1");
					responseVideoDetails.add(objresponse);
					
				}
				dataMap.put("message", "Video Details found successfully");
				dataMap.put("status", "1");
				dataMap.put("videoDetails", responseVideoDetails);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Video Details");
				dataMap.put("status", "1");
				dataMap.put("videoDetails", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Video Details");
			dataMap.put("status", "1");
			dataMap.put("videoDetails", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-module-video")
	public Map<String, Object> readAll() {
		List<VideoDetails> videoDetails = videoRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Video Details\");\r\n" + 
				"			dataMap.put(\"status\", \"1\"); found successfully");
		dataMap.put("totalmoduleType", videoDetails.size());
		dataMap.put("status", "1");
		dataMap.put("videoDetails", videoDetails);
	    return dataMap;
	}
}
