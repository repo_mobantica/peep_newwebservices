package com.peep1.controller;

import java.security.spec.KeySpec;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.PatientInformation;
import com.peep1.repository.PatientRepository;
import com.peep1.response.LoginDetails;

@RestController
public class commonController {
	private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;
    
    @Autowired
	PatientRepository patientRespository;
    
    public commonController() throws Exception {
        myEncryptionKey = "PEEPProjectEncryptiondecryption";
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }

	 public String encrypt(String unencryptedString) {
	        String encryptedString = null;
	        try {
	            cipher.init(Cipher.ENCRYPT_MODE, key);
	            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
	            byte[] encryptedText = cipher.doFinal(plainText);
	            encryptedString = new String(Base64.encodeBase64(encryptedText));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return encryptedString;
	    }


	    public String decrypt(String encryptedString) {
	        String decryptedText=null;
	        try {
	            cipher.init(Cipher.DECRYPT_MODE, key);
	            byte[] encryptedText = Base64.decodeBase64(encryptedString);
	            byte[] plainText = cipher.doFinal(encryptedText);
	            decryptedText= new String(plainText);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return decryptedText;
	    }
	    
	    
public boolean checkLogin(LoginDetails loginDetails) {
			
			commonController common;
			
			String ecryptedPassword = null;
	    	
			try {
				 common = new commonController();
				 ecryptedPassword=common.encrypt(loginDetails.getPassword());
			//	 System.out.println(loginDetails.getUsername());
				 PatientInformation p= patientRespository.findByusernameAndPassword(loginDetails.getUsername(), ecryptedPassword);
				  if(p != null && p.getUsername().equals(loginDetails.getUsername()))
	     		  {
					  return true;
					
				  }
				
			}
			
				catch(Exception ex)
				{
					
						System.out.println(ex.getMessage());	
				}
				return false;
}

public PatientInformation getUserId(LoginDetails loginDetails) {
	commonController common;
	
	String ecryptedPassword = null;
	
	try {
		 common = new commonController();
		 ecryptedPassword=common.encrypt(loginDetails.getPassword());
	//	 System.out.println(loginDetails.getUsername());
		 PatientInformation p= patientRespository.findByusernameAndPassword(loginDetails.getUsername(), ecryptedPassword);
		  if(p != null && p.getUsername().equals(loginDetails.getUsername()))
 		  {
			  return p;
			
		  }
		
	}
	
		catch(Exception ex)
		{
			
				System.out.println(ex.getMessage());	
		}
		return null;
}


}
