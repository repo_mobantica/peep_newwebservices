package com.peep1.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.entity.PatientInformation;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.QuestionDetailsUserwise;
import com.peep1.entity.RedFlagOptionDetails;
import com.peep1.entity.RedFlagQuestionDetails;
import com.peep1.entity.RedFlagSymptomsUserWise;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.QuestionsRepository;
import com.peep1.repository.RedFlagOptionRepository;
import com.peep1.repository.RedFlagQuestionsRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponceWSOption;
import com.peep1.response.ResponceWSQuestions;
import com.peep1.response.ResponceWSRedFlagOption;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseQuestionDetails;
import com.peep1.response.ResponseRedFlagQuestionDetails;
import com.peep1.response.ResponseRedFlagSymptomsData;

import ch.qos.logback.core.net.SyslogOutputStream;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/redFlagQuestions")
public class RedFlagQuestionsController {
	
	@Autowired
	RedFlagQuestionsRepository redFlagQuestionsRepository;
	
	@Autowired
	RedFlagOptionRepository redFlagOptionRepository;
	
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  RedFlagQuestionDetails redFlagQuestionDetails) {
		
		redFlagQuestionDetails.setCreatedDate(new Date());
		redFlagQuestionDetails.setStatus("1");
		redFlagQuestionDetails = redFlagQuestionsRepository.save(redFlagQuestionDetails);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Red flag Questions created successfully");
		dataMap.put("status", "1");
		dataMap.put("redFlagQuestionDetails", redFlagQuestionDetails);
	    return dataMap;
	}

//read active cancer types
	@RequestMapping(method=RequestMethod.POST,value="/getRedFlagQuestionDetails")
	public Map<String, Object> getQuestionList(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<RedFlagQuestionDetails> questionList = redFlagQuestionsRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseRedFlagQuestionDetails> responseQuestion=new ArrayList<>();
				for(int i=0;i<questionList.size();i++)
				{
					ResponseRedFlagQuestionDetails objresponse=new ResponseRedFlagQuestionDetails();
					objresponse.setRedFlagQuestionId(questionList.get(i).getRedFlagQuestionId());
					objresponse.setRedFlagQuestionName(questionList.get(i).getRedFlagQuestionName());
					objresponse.setCorrectOptionId(questionList.get(i).getCorrectOptionId());
					responseQuestion.add(objresponse);
					
				}
				dataMap.put("message", "Question Details found successfully");
				dataMap.put("status", "1");
				dataMap.put("questionList", responseQuestion);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Question Details");
				dataMap.put("status", "1");
				dataMap.put("questionList", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Question Details");
			dataMap.put("status", "1");
			dataMap.put("questionList", null);
		}
	    return dataMap;
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/getRedFlagSymptoms")
	public Map<String, Object> getRedFlagSymptoms(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<RedFlagQuestionDetails> questionList = redFlagQuestionsRepository.findByStatus("1");
			
			
			List<ResponseRedFlagSymptomsData> resonseSymptoms = new ArrayList<ResponseRedFlagSymptomsData>();
		
			
			if(c.checkLogin(loginDetails))
			{

				for(int k=0;k<questionList.size();k++)
				{	
					
					List<ResponceWSRedFlagOption> wsOptionList = new ArrayList<ResponceWSRedFlagOption>();
					ResponseRedFlagSymptomsData responceWSQuestions= new ResponseRedFlagSymptomsData();
					responceWSQuestions.setRedFlagQuestionId(questionList.get(k).getRedFlagQuestionId());
					responceWSQuestions.setRedFlagQuestionName(questionList.get(k).getRedFlagQuestionName());
					responceWSQuestions.setCorrectOptionId(questionList.get(k).getCorrectOptionId());
					//System.out.println(questionList.get(k).getRedFlagQuestionId());
					List<RedFlagOptionDetails> optionDetails=redFlagOptionRepository.findByStatusAndRedFlagQuestionId("1",questionList.get(k).getRedFlagQuestionId());
					
				
					for(int y=0;y<optionDetails.size();y++)
					{
						//System.out.println(questionList.get(k).getRedFlagQuestionId() +" "+optionDetails.get(y).getRedFlagOptionName());
						
						ResponceWSRedFlagOption responceWSOption=new ResponceWSRedFlagOption();
						responceWSOption.setRedFlagOptionId(optionDetails.get(y).getRedFlagOptionId());
						responceWSOption.setRedFlagOptionName(optionDetails.get(y).getRedFlagOptionName());
						
						wsOptionList.add(responceWSOption);
						
					}
					responceWSQuestions.setWsRedflagOption(wsOptionList);
					resonseSymptoms.add(responceWSQuestions);
				}
			
				dataMap.put("message", "RedFlag Questions found successfully");
				dataMap.put("status", "1");
				dataMap.put("resonseSymptoms", resonseSymptoms);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving RedFlag Questions");
				dataMap.put("status", "1");
				dataMap.put("resonseSymptoms", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving  RedFlag Questions");
			dataMap.put("status", "1");
			dataMap.put("resonseSymptoms", null);
		}
	    return dataMap;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-questions")
	public Map<String, Object> readAll() {
		List<RedFlagQuestionDetails> redFlagQuestionDetails = redFlagQuestionsRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Red Flag Question Details found successfully");
		dataMap.put("totalquestions", redFlagQuestionDetails.size());
		dataMap.put("status", "1");
		dataMap.put("redFlagQuestionDetails", redFlagQuestionDetails);
	    return dataMap;
	}
}
