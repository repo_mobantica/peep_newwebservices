package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.FeedBackOption;
import com.peep1.entity.FeedBackUserWise;
import com.peep1.entity.ModuleType;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.FeedBackRepository;
import com.peep1.repository.FeedBackUserWiseRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseFeedBack;
import com.peep1.response.ResponseModuleType;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/feedback")
public class FeedBackController {
	
	@Autowired
	FeedBackRepository feedBackRepository;
	
	
	@Autowired
	FeedBackUserWiseRepository feedBackUserWiseRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  FeedBackOption feedBack) {
		
		feedBack.setCreatedDate(new Date());
		feedBack.setStatus("1");
		feedBack = feedBackRepository.save(feedBack);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Feedback created successfully");
		dataMap.put("status", "1");
		dataMap.put("feedBack", feedBack);
	    return dataMap;
	}

	
	
	
	@RequestMapping(method=RequestMethod.POST, value="/addFeedBack")
   	public Map<String, Object> addFeedBack(@RequestBody  FeedBackUserWise feedBackUserWise) {
		feedBackUserWise.setUserId(feedBackUserWise.getUserId());
		feedBackUserWise.setCreatedDate(new Date());
		feedBackUserWise.setStatus("1");
		feedBackUserWise = feedBackUserWiseRepository.save(feedBackUserWise);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Feedback submitted successfully");
		dataMap.put("status", "1");
		dataMap.put("feedBackUserWise", feedBackUserWise);
	    return dataMap;
	}
	

	@RequestMapping(method=RequestMethod.POST,value="/getAllFeedBackOptionList")
	public Map<String, Object> getModuleTypes(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<FeedBackOption> feedBack = feedBackRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseFeedBack> responseFeedBack=new ArrayList<>();
				for(int i=0;i<feedBack.size();i++)
				{
					ResponseFeedBack objresponse=new ResponseFeedBack();
					objresponse.setFeedBackId(responseFeedBack.get(i).getFeedBackId());
					objresponse.setFeedBackOptionName(responseFeedBack.get(i).getFeedBackOptionName());
					
					responseFeedBack.add(objresponse);
					
				}
				dataMap.put("message", "Feed Back found successfully");
				dataMap.put("status", "1");
				dataMap.put("feedBack", responseFeedBack);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Feed Back Option List");
				dataMap.put("status", "1");
				dataMap.put("moduleType", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Feed Back Option List");
			dataMap.put("status", "1");
			dataMap.put("moduleType", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-feedBack")
	public Map<String, Object> readAll() {
		List<FeedBackOption> feedBack = feedBackRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Feed Back found successfully");
		dataMap.put("totalFeedBackOptions", feedBack.size());
		dataMap.put("status", "1");
		dataMap.put("feedBack", feedBack);
	    return dataMap;
	}
}
