package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponseCancerType;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/cancerType")
public class CancerTypeController {
	
	@Autowired
	CancerTypeRepository cancerTypeRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  CancerType cancerType) {
		
		cancerType.setStatus("1");
		cancerType.setCreatedDate(new Date());
		cancerType = cancerTypeRepository.save(cancerType);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Cancer Type created successfully");
		dataMap.put("status", "1");
		dataMap.put("cancerType", cancerType);
	    return dataMap;
	}

//read active cancer types
	@RequestMapping(method=RequestMethod.POST,value="/getCancerTypes")
	public Map<String, Object> getCancerTypes(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<CancerType> cancerType = cancerTypeRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseCancerType> responseCancerType=new ArrayList<>();
				for(int i=0;i<cancerType.size();i++)
				{
					ResponseCancerType objresponse=new ResponseCancerType();
					objresponse.setCancerType(cancerType.get(i).getCancerType());
					objresponse.setId(cancerType.get(i).getId());
					responseCancerType.add(objresponse);
					
				}
				dataMap.put("message", "Cancer Type found successfully");
				dataMap.put("status", "1");
				dataMap.put("cancerType", responseCancerType);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Cancer Type");
				dataMap.put("status", "1");
				dataMap.put("cancerType", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Cancer Type");
			dataMap.put("status", "1");
			dataMap.put("cancerType", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all")
	public Map<String, Object> readAll() {
		//Pageable p
		List<CancerType> cancerType = cancerTypeRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Cancer Type found successfully");
		dataMap.put("totalcancerType", cancerType.size());
		dataMap.put("status", "1");
		dataMap.put("cancerType", cancerType);
	    return dataMap;
	}
}
