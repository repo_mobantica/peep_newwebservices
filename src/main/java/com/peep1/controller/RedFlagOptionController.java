package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.OptionDetails;
import com.peep1.entity.RedFlagOptionDetails;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.OptionRepository;
import com.peep1.repository.RedFlagOptionRepository;
import com.peep1.repository.RedFlagQuestionsRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;
import com.peep1.response.ResponseOptionDetails;
import com.peep1.response.ResponseRedFlagOptionDetails;
import com.peep1.response.ResponseRedFlagQuestionDetails;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */

@RestController
@RequestMapping("/redFlagOption")
public class RedFlagOptionController {
	
	@Autowired
	RedFlagOptionRepository redFlagOptionRepository;
	
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  RedFlagOptionDetails redFlagOptionDetails) {
		
		redFlagOptionDetails.setCreatedDate(new Date());
		redFlagOptionDetails.setStatus("1");
		redFlagOptionDetails = redFlagOptionRepository.save(redFlagOptionDetails);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Option Details created successfully");
		dataMap.put("status", "1");
		dataMap.put("redFlagOptionDetails", redFlagOptionDetails);
	    return dataMap;
	}

//read active cancer types
	@RequestMapping(method=RequestMethod.POST,value="/getRedFlagOptionDetails")
	public Map<String, Object> getRedFlagOptionDetails(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<RedFlagOptionDetails> optionDetails = redFlagOptionRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseRedFlagOptionDetails> responseOptionDetails=new ArrayList<>();
				for(int i=0;i<optionDetails.size();i++)
				{
					ResponseRedFlagOptionDetails objresponse=new ResponseRedFlagOptionDetails();
					objresponse.setRedFlagOptionId(optionDetails.get(i).getRedFlagOptionId());
					objresponse.setRedFlagOptionName((optionDetails.get(i).getRedFlagOptionName()));
					objresponse.setRedFlagQuestionId((optionDetails.get(i).getRedFlagQuestionId()));
					objresponse.setStatus((optionDetails.get(i).getStatus()));
					responseOptionDetails.add(objresponse);
					
				}
				dataMap.put("message", "Option Details found successfully");
				dataMap.put("status", "1");
				dataMap.put("redFlagOptionDetails", responseOptionDetails);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Option Details");
				dataMap.put("status", "1");
				dataMap.put("redFlagOptionDetails", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Option Details");
			dataMap.put("status", "1");
			dataMap.put("redFlagOptionDetails", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-module")
	public Map<String, Object> readAll() {
		List<RedFlagOptionDetails> optionDetails = redFlagOptionRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Option Details found successfully");
		dataMap.put("totalOptions", optionDetails.size());
		dataMap.put("status", "1");
		dataMap.put("redFlagOptionDetails", optionDetails);
	    return dataMap;
	}
}
