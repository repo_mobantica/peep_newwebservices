package com.peep1.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.peep1.entity.ActivityDetails;
import com.peep1.entity.Answers;
import com.peep1.entity.Booking;
import com.peep1.entity.CancerType;
import com.peep1.entity.ModuleType;
import com.peep1.entity.PatientInformation;
import com.peep1.entity.QuestionDetails;
import com.peep1.entity.VideoDetails;
import com.peep1.entity.VideoDetailsUserwise;
import com.peep1.repository.ActivityRepository;
import com.peep1.repository.AnswersRepository;
import com.peep1.repository.CancerTypeRepository;
import com.peep1.repository.ModuleTypeRepository;
import com.peep1.repository.OptionRepository;
import com.peep1.repository.PatientRepository;
import com.peep1.repository.QuestionsRepository;
import com.peep1.repository.QuestionsUserWiseRepository;
import com.peep1.repository.VideoRepository;
import com.peep1.repository.VideoUserwiseRepository;
import com.peep1.response.LoginDetails;
import com.peep1.response.ResponseCancerType;
import com.peep1.response.ResponseModuleType;



/**
 * @author Savita Sutar
 * Date : 26-4-2018
 */
@RestController
@RequestMapping("/moduleType")
public class ModuleTypeController {
	
	@Autowired
	ActivityRepository activityRepository;
	
    @Autowired
	PatientRepository patientRespository;
	
	@Autowired
	ModuleTypeRepository moduleTypeRepository;
	
	@Autowired
	VideoRepository videoRepository;
	
	@Autowired
	QuestionsRepository questionRepository;

	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	AnswersRepository answersRepository;
	
	@Autowired
	VideoUserwiseRepository videoUserWiseRepository;
	
	
	@Autowired
	QuestionsUserWiseRepository questionsUserWiseRepository;
	@Autowired
	commonController c;
	  
	/**
	 * 
	 * 
	 * GET /create  --> Create a new cancer type and save it in the database.
	 */
	@RequestMapping(method=RequestMethod.POST, value="/create")
   	public Map<String, Object> create(@RequestBody  ModuleType moduleType) {
		///System.out.println("Module Type");
		moduleType.setCreatedDate(new Date());
		moduleType.setStatus("1");
		moduleType = moduleTypeRepository.save(moduleType);
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Module Type created successfully");
		dataMap.put("status", "1");
		dataMap.put("moduleType", moduleType);
	    return dataMap;
	}

//read active cancer types
	@RequestMapping(method=RequestMethod.POST,value="/getModuleTypes")
	public Map<String, Object> getModuleTypes(@RequestBody LoginDetails loginDetails) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		try
		{
			List<ModuleType> moduleType = moduleTypeRepository.findByStatus("1");
			if(c.checkLogin(loginDetails))
			{
				List<ResponseModuleType> responseModuleType=new ArrayList<>();
				int finalTotalPer=0;
				int dashboardPer=0;
				int totalDashboardPer=0;
				
				int videoCount=0;
				int questionCount=0;
				int watchQuestionCount=0;
				int watchVideoCount=0;
				int totalPer=0;
				int per=0;
				for(int i=0;i<moduleType.size();i++)
				{
					dashboardPer=0;
					finalTotalPer=0;
					ResponseModuleType objresponse=new ResponseModuleType();
					objresponse.setModuleType(moduleType.get(i).getModuleType());
					objresponse.setId(moduleType.get(i).getId());
					
					List<ActivityDetails> activities = activityRepository.findByStatusAndModuleId("1", moduleType.get(i).getId());
					String ecryptedPassword = null;
				
					try {
						commonController common = new commonController();
						 ecryptedPassword=common.encrypt(loginDetails.getPassword());
					//	 System.out.println(loginDetails.getUsername());
						 PatientInformation p= patientRespository.findByusernameAndPassword(loginDetails.getUsername(), ecryptedPassword);
						
						 List<VideoDetails> videoDetails=new ArrayList<VideoDetails>();
						 List<QuestionDetails>questionDetails=new ArrayList<QuestionDetails>();
						 List<Answers> answerList=new  ArrayList<Answers>();
						 List<VideoDetailsUserwise> videoDetailsUserwise=new ArrayList<VideoDetailsUserwise>();
							for(int k=0;k<activities.size();k++)
							{
								totalPer=0;
								 videoCount=0;
								 questionCount=0;
								 watchQuestionCount=0;
								 watchVideoCount=0;
								 
								videoDetails = videoRepository.findByStatusAndActivityId("1", activities.get(k).getActivityId());
								 questionDetails = questionRepository.findByStatusAndActivityId("1", activities.get(k).getActivityId());
								 
								 if(videoDetails.size()>0)
								 {
									 videoCount=videoDetails.size();
								 }
								activities.get(k).setVideoCount(videoCount);
								 if(questionDetails.size()>0)
								 {
									 questionCount=questionDetails.size();
								 }
						
						for(int y=0;y<questionDetails.size();y++)
						{
							 answerList = questionsUserWiseRepository.findByStatusAndActivityIdAndQuestionIdAndUserId("1",activities.get(k).getActivityId(),
									questionDetails.get(y).getQuestionId(),p.getId());
							 if(answerList.size()>0)
							 {
								 watchQuestionCount=watchQuestionCount+1;
							 }
						}	 
							videoDetailsUserwise = videoUserWiseRepository.findByStatusAndActivityIdAndUserId("1",activities.get(k).getActivityId(),p.getId());
							 if(videoDetailsUserwise.size()>0)
							 {
								 watchVideoCount=videoDetailsUserwise.size();
							 }
								
						
						
						try {
						if((videoCount+questionCount)!=0)
						{
							totalPer=(100*(watchQuestionCount+watchVideoCount)/(videoCount+questionCount)) ;
							finalTotalPer=finalTotalPer+totalPer;
						}
						}catch (Exception e) {
							System.out.println(" error  = "+e);
						}
						
						
					}
					
					}catch(Exception e)
					{
						
					}
					
					if(activities.size()!=0)
					{
						dashboardPer=finalTotalPer/activities.size();
						totalDashboardPer=totalDashboardPer+dashboardPer;
					}
					objresponse.setFinalTotalPer(dashboardPer);
					responseModuleType.add(objresponse);
					
				}
				if(moduleType.size()!=0)
				{
					per=totalDashboardPer/moduleType.size();
				}
				
				dataMap.put("final_dash_per", per);
				dataMap.put("message", "Module Type found successfully");
				dataMap.put("status", "1");
				dataMap.put("moduleType", responseModuleType);
				}
			else
			{
				
				dataMap.put("message", "erro while retriving Module Type");
				dataMap.put("status", "1");
				dataMap.put("moduleType", null);
			}
		}
		catch(Exception ex)
		{			
			dataMap.put("message", "erro while retriving Module Type");
			dataMap.put("status", "1");
			dataMap.put("moduleType", null);
		}
	    return dataMap;
	}

	/**
	 * GET /read  --> Read all booking from the database.
	 */
	@RequestMapping("/read-all-module")
	public Map<String, Object> readAll() {
		List<ModuleType> moduleType = moduleTypeRepository.findAll();
		//List<Booking> bookings = bookingRepository.findELemtsNamehhh();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("message", "Module Type found successfully");
		dataMap.put("totalmoduleType", moduleType.size());
		dataMap.put("status", "1");
		dataMap.put("moduleType", moduleType);
	    return dataMap;
	}
}
