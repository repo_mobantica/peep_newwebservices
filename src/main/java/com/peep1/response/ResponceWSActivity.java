package com.peep1.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponceWSActivity {

	private List<ResponceWSVideo> wsVideo=new ArrayList<ResponceWSVideo>();
	private List<ResponceWSQuestions> wsQuestions=new ArrayList<ResponceWSQuestions>();

	String activityName;
	String status;
	String moduleId;
	String activityId;
	
	public String getActivityId() {
		return activityId;
	}
	public List<ResponceWSVideo> getWsVideo() {
		return wsVideo;
	}
	public void setWsVideo(List<ResponceWSVideo> wsVideo) {
		this.wsVideo = wsVideo;
	}
	public List<ResponceWSQuestions> getWsQuestions() {
		return wsQuestions;
	}
	public void setWsQuestions(List<ResponceWSQuestions> wsQuestions) {
		this.wsQuestions = wsQuestions;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	


	
	
	

}
