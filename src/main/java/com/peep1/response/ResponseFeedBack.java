package com.peep1.response;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponseFeedBack {

	
	
	String feedBackId;
	String feedBackOptionName;
	String status;
	Date createdDate;
	Date updatedDate;

	public String getFeedBackId() {
		return feedBackId;
	}
	public void setFeedBackId(String feedBackId) {
		this.feedBackId = feedBackId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public String getFeedBackOptionName() {
		return feedBackOptionName;
	}
	public void setFeedBackOptionName(String feedBackOptionName) {
		this.feedBackOptionName = feedBackOptionName;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
