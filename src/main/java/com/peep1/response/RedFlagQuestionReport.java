package com.peep1.response;


import java.util.ArrayList;
import java.util.List;

public class RedFlagQuestionReport {

	String id;
	String date;
	String optionName;
	
	int redFlagStatus ;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public int getRedFlagStatus() {
		return redFlagStatus;
	}

	public void setRedFlagStatus(int redFlagStatus) {
		this.redFlagStatus = redFlagStatus;
	}

	
}
