package com.peep1.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponseRedFlagQuestinAnswerDetailsUserWise {

	List<WsRedFlagQuestionDetailsUserwise > addQuestionAnswer=new ArrayList<WsRedFlagQuestionDetailsUserwise >();
	private  int user_id;
	private String created_date;
	public List<WsRedFlagQuestionDetailsUserwise> getAddQuestionAnswer() {
		return addQuestionAnswer;
	}
	public void setAddQuestionAnswer(List<WsRedFlagQuestionDetailsUserwise> addQuestionAnswer) {
		this.addQuestionAnswer = addQuestionAnswer;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	
	



	

}
