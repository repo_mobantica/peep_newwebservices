package com.peep1.response;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponceWSRedFlagOption {

	
	String redFlagOptionId;
	String redFlagOptionName;

	public String getRedFlagOptionId() {
		return redFlagOptionId;
	}

	public void setRedFlagOptionId(String redFlagOptionId) {
		this.redFlagOptionId = redFlagOptionId;
	}

	public String getRedFlagOptionName() {
		return redFlagOptionName;
	}

	public void setRedFlagOptionName(String redFlagOptionName) {
		this.redFlagOptionName = redFlagOptionName;
	}


	
	

}
