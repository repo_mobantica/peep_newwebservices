package com.peep1.response;

import java.util.ArrayList;
import java.util.List;

public class RedFlagReport {

	private List<RedFlagQuestionReport> arrayOfReport=new ArrayList<RedFlagQuestionReport>();
	
	String questionId;
	String questionName;
	public List<RedFlagQuestionReport> getArrayOfReport() {
		return arrayOfReport;
	}
	public void setArrayOfReport(List<RedFlagQuestionReport> arrayOfReport) {
		this.arrayOfReport = arrayOfReport;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	
	
}
