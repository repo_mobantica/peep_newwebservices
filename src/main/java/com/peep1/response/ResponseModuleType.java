package com.peep1.response;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponseModuleType {
	String id;
	String moduleType;
	int FinalTotalPer;
	public String getId() {
		return id;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getFinalTotalPer() {
		return FinalTotalPer;
	}
	public void setFinalTotalPer(int finalTotalPer) {
		FinalTotalPer = finalTotalPer;
	}

}
