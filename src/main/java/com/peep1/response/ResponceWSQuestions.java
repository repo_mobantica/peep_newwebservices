package com.peep1.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponceWSQuestions {

	private List<ResponceWSOption> wsOption=new ArrayList<ResponceWSOption>();
	String questionId;
	String questionName;
	String status;
	String correctOptionId;
	String answerId;


	public String getAnswerId() {
		return answerId;
	}
	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public List<ResponceWSOption> getWsOption() {
		return wsOption;
	}
	public void setWsOption(List<ResponceWSOption> wsOption) {
		this.wsOption = wsOption;
	}

	public String getCorrectOptionId() {
		return correctOptionId;
	}
	public void setCorrectOptionId(String correctOptionId) {
		this.correctOptionId = correctOptionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
	
	

}
