package com.peep1.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponseActivityData {
	
	
	private List<ResponceWSVideo> wsVideo=new ArrayList<ResponceWSVideo>();
	private List<ResponceWSQuestions> wsQuestions=new ArrayList<ResponceWSQuestions>();

	String activityName;
	String status;
	String moduleId;
	String activityId;
	int videoCount;
	int questionCount;
	int watchVideoCount;
	int watchQuestionCount;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public int getWatchVideoCount() {
		return watchVideoCount;
	}
	public void setWatchVideoCount(int watchVideoCount) {
		this.watchVideoCount = watchVideoCount;
	}
	public int getWatchQuestionCount() {
		return watchQuestionCount;
	}
	public void setWatchQuestionCount(int watchQuestionCount) {
		this.watchQuestionCount = watchQuestionCount;
	}
	public String getActivityId() {
		return activityId;
	}
	public List<ResponceWSVideo> getWsVideo() {
		return wsVideo;
	}
	public void setWsVideo(List<ResponceWSVideo> wsVideo) {
		this.wsVideo = wsVideo;
	}
	public List<ResponceWSQuestions> getWsQuestions() {
		return wsQuestions;
	}
	public void setWsQuestions(List<ResponceWSQuestions> wsQuestions) {
		this.wsQuestions = wsQuestions;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public int getVideoCount() {
		return videoCount;
	}
	public void setVideoCount(int videoCount) {
		this.videoCount = videoCount;
	}
	public int getQuestionCount() {
		return questionCount;
	}
	public void setQuestionCount(int questionCount) {
		this.questionCount = questionCount;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*String  username;
	String password;
	String pageNo;
	String moduleId;
	private List<ResponceWSActivity> wsActivity=new ArrayList<ResponceWSActivity>();
	private List<ResponceWSVideo> wsVideo = new ArrayList<ResponceWSVideo>();
	private List<ResponceWSQuestions> wsQuestion= new ArrayList<ResponceWSQuestions>();
	private List<ResponceWSOption> wsOption = new ArrayList<ResponceWSOption>();
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<ResponceWSVideo> getWsVideo() {
		return wsVideo;
	}
	public void setWsVideo(List<ResponceWSVideo> wsVideo) {
		this.wsVideo = wsVideo;
	}
	public List<ResponceWSQuestions> getWsQuestion() {
		return wsQuestion;
	}
	public void setWsQuestion(List<ResponceWSQuestions> wsQuestion) {
		this.wsQuestion = wsQuestion;
	}
	public List<ResponceWSOption> getWsOption() {
		return wsOption;
	}
	public void setWsOption(List<ResponceWSOption> wsOption) {
		this.wsOption = wsOption;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPageNo() {
		return pageNo;
	}
	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public List<ResponceWSActivity> getWsActivity() {
		return wsActivity;
	}
	public void setWsActivity(List<ResponceWSActivity> wsActivity) {
		this.wsActivity = wsActivity;
	}*/





}
