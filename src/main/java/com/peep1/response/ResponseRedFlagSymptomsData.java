package com.peep1.response;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

public class ResponseRedFlagSymptomsData {

	
	 String redFlagQuestionId;
	 String redFlagQuestionName;
	 String correctOptionId;
	private List<ResponceWSRedFlagOption> wsRedflagOption = new ArrayList<ResponceWSRedFlagOption>();
	

	public List<ResponceWSRedFlagOption> getWsRedflagOption() {
		return wsRedflagOption;
	}
	public void setWsRedflagOption(List<ResponceWSRedFlagOption> wsRedflagOption) {
		this.wsRedflagOption = wsRedflagOption;
	}
	public String getRedFlagQuestionId() {
		return redFlagQuestionId;
	}
	public void setRedFlagQuestionId(String redFlagQuestionId) {
		this.redFlagQuestionId = redFlagQuestionId;
	}
	public String getRedFlagQuestionName() {
		return redFlagQuestionName;
	}
	public void setRedFlagQuestionName(String redFlagQuestionName) {
		this.redFlagQuestionName = redFlagQuestionName;
	}
	public String getCorrectOptionId() {
		return correctOptionId;
	}
	public void setCorrectOptionId(String correctOptionId) {
		this.correctOptionId = correctOptionId;
	}

	
	

}
