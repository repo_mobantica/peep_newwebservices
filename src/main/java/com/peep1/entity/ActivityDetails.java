package com.peep1.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class ActivityDetails {

	@Id
	String activityId;
	String activityName;
	Date createdDate;
	Date updatedDate;
	String status;
	String moduleId;
	@Transient
	int videoCount;
	
	@Transient
	int questionCount;
	
	@Transient
	int watchQuestionCount;
	
	@Transient
	int watchVideoCount;
	
	@Transient
	int totalPer;
	
	
	public int getVideoCount() {
		return videoCount;
	}
	public void setVideoCount(int videoCount) {
		this.videoCount = videoCount;
	}
	public int getQuestionCount() {
		return questionCount;
	}
	public void setQuestionCount(int questionCount) {
		this.questionCount = questionCount;
	}
	public int getWatchQuestionCount() {
		return watchQuestionCount;
	}
	public void setWatchQuestionCount(int watchQuestionCount) {
		this.watchQuestionCount = watchQuestionCount;
	}
	public int getWatchVideoCount() {
		return watchVideoCount;
	}
	public void setWatchVideoCount(int watchVideoCount) {
		this.watchVideoCount = watchVideoCount;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getTotalPer() {
		return totalPer;
	}
	public void setTotalPer(int totalPer) {
		this.totalPer = totalPer;
	}


}
