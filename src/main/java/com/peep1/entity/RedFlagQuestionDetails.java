package com.peep1.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class RedFlagQuestionDetails {

	
	@Id
	String redFlagQuestionId;
	String redFlagQuestionName;
	Date createdDate;
	Date updatedDate;
	String status;
	String correctOptionId;



	public String getRedFlagQuestionId() {
		return redFlagQuestionId;
	}
	public void setRedFlagQuestionId(String redFlagQuestionId) {
		this.redFlagQuestionId = redFlagQuestionId;
	}
	public String getRedFlagQuestionName() {
		return redFlagQuestionName;
	}
	public void setRedFlagQuestionName(String redFlagQuestionName) {
		this.redFlagQuestionName = redFlagQuestionName;
	}
	public String getCorrectOptionId() {
		return correctOptionId;
	}
	public void setCorrectOptionId(String correctOptionId) {
		this.correctOptionId = correctOptionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
