package com.peep1.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class RedFlagOptionDetails {

	
	@Id
	String redFlagOptionId;
	
	String redFlagOptionName;
	Date createdDate;
	Date updatedDate;
	String status;
	String redFlagQuestionId;


	public String getRedFlagOptionId() {
		return redFlagOptionId;
	}
	public void setRedFlagOptionId(String redFlagOptionId) {
		this.redFlagOptionId = redFlagOptionId;
	}
	public String getRedFlagOptionName() {
		return redFlagOptionName;
	}
	public void setRedFlagOptionName(String redFlagOptionName) {
		this.redFlagOptionName = redFlagOptionName;
	}
	
	public String getRedFlagQuestionId() {
		return redFlagQuestionId;
	}
	public void setRedFlagQuestionId(String redFlagQuestionId) {
		this.redFlagQuestionId = redFlagQuestionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
