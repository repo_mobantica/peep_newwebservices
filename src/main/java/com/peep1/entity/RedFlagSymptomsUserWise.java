package com.peep1.entity;


import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class RedFlagSymptomsUserWise {

	
	@Id
	String Id;
	String redFlagQuestionId;
	String redFlagAnswerId;
	String userId;
	Date createdDate;
	Date updatedDate;
	String status;
	String correctOptionId;
	
	@Transient
	String redFlagQuestionName;
	
	@Transient
	String createdDate1;
	
	@Transient
	String answer;
	
	
	public String getCreatedDate1() {
		return createdDate1;
	}
	public void setCreatedDate1(String createdDate1) {
		this.createdDate1 = createdDate1;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getRedFlagQuestionName() {
		return redFlagQuestionName;
	}
	public void setRedFlagQuestionName(String redFlagQuestionName) {
		this.redFlagQuestionName = redFlagQuestionName;
	}
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getRedFlagQuestionId() {
		return redFlagQuestionId;
	}
	public void setRedFlagQuestionId(String redFlagQuestionId) {
		this.redFlagQuestionId = redFlagQuestionId;
	}
	public String getRedFlagAnswerId() {
		return redFlagAnswerId;
	}
	public void setRedFlagAnswerId(String redFlagAnswerId) {
		this.redFlagAnswerId = redFlagAnswerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCorrectOptionId() {
		return correctOptionId;
	}
	public void setCorrectOptionId(String correctOptionId) {
		this.correctOptionId = correctOptionId;
	}



	

}
